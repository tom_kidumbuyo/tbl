import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { AuthenticationService } from '../_services/authentication.service';
import { RestApiService } from '../_services/rest-api.service';

@Component({
  selector: 'app-activation',
  templateUrl: './activation.component.html',
  styleUrls: ['./activation.component.scss']
})
export class ActivationComponent implements OnInit {


  constructor(
    private auth: AuthenticationService,
    private router: Router,
    private restApi: RestApiService,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private dialog: MatDialog
  ) { 

    this.restApi.getAuth('outlet')
    .then((data: any) => {

    })
    .catch((err) => {
      this.snackBar.open(err.error.message, 'Close', {
        verticalPosition: 'top'
      });
    });

  }


  ngOnInit(): void {

  }

}

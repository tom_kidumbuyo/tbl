import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject, of, Observable, from } from 'rxjs';
import { RestApiService } from './rest-api.service';
import { resolve } from 'url';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private dataSource = new Subject();
  private page = 'profile';
  private user;

  constructor(
    private restApi: RestApiService,
  ) {
    this.getUser();
  }

  getUser() {
    this.restApi.getAuth('user')
    .then(data => {
      this.user = data;
      this.sendData();
    })
    .catch(err => {
      console.log('error getting user', err)
    })
  }


  setPage(page) {
    this.page = page;
    this.sendData();
  }

  sendData() {
    this.dataSource.next({
      page: this.page,
      user: this.user,
    })
  }

  getDataObservable() {
    return this.dataSource;
  }

  update(data) {
    return new Promise(
      (resolve, reject) => {
        this.restApi.putAuth('user/update', data)
        .then(data => {
          this.getUser();
          resolve(data);
        })
        .catch(err => {
          console.log(err);
          reject(err);
        })
      }
    )
  }

  autoCompleteByEmail(value){
    return from(this.restApi.getAuth('user/emailAutocomplete?query=' + value));
  }

}

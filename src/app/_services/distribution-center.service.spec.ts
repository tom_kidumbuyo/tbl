import { TestBed } from '@angular/core/testing';

import { DistributionCenterService } from './distribution-center.service';

describe('DistributionCenterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DistributionCenterService = TestBed.get(DistributionCenterService);
    expect(service).toBeTruthy();
  });
});

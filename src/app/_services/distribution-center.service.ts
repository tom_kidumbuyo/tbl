import { Injectable } from '@angular/core';
import { RestApiService } from './rest-api.service';
import { Subject } from 'rxjs';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class DistributionCenterService {

  private dataSource = new Subject();
  page: string;
  distributionCenter: any;
  user: any;
  region: any;

  constructor(
    private restApi: RestApiService,
    private auth: AuthenticationService,
  ) {
    this.auth.isLoggedIn()
    .then(data => {
      if (data) {
        this.user = data;
        this.restApi.get('distributionCenter/' + this.user.dc)
        .then(dc => {
          this.distributionCenter = dc;
          this.sendData();
          this.restApi.get('region/' + this.distributionCenter._id)
          .then(region => {
            this.region = region;
            this.sendData();
          })
          .catch(err => {
            console.log('regionalSupervisorService', 'Error getting regions.')
          });
        })
        .catch(err => {
          console.log('regionalSupervisorService', 'Error getting regions.')
        })
      }
    })
    .catch(err => {
      console.log('regionalSupervisorService', 'Looks like you are not logged in.')
    })

  }

  getDataObservable() {
    return this.dataSource;
  }

  setPage(page) {
    this.page = page;
    this.sendData();
  }

  sendData() {
    this.dataSource.next({
      page: this.page
    });
  }
}

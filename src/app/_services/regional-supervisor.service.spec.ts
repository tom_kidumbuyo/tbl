import { TestBed } from '@angular/core/testing';

import { RegionalSupervisorService } from './regional-supervisor.service';

describe('RegionalSupervisorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RegionalSupervisorService = TestBed.get(RegionalSupervisorService);
    expect(service).toBeTruthy();
  });
});

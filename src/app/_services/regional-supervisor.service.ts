import { Injectable } from '@angular/core';
import { RestApiService } from './rest-api.service';
import { Subject } from 'rxjs';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class RegionalSupervisorService {

  private dataSource = new Subject();
  page: string;
  region: any;
  user: any;

  constructor(
    private restApi: RestApiService,
    private auth: AuthenticationService,
  ) {
    this.auth.isLoggedIn()
    .then(data => {
      if (data) {
        this.user = data;
        this.restApi.getAuth('region/' + this.user.region + '/report')
        .then((region: any) => {
            console.log(region);
            this.region = region;
            this.processData();
        })
        .catch(err => {
            console.log('Error getting region report.', err);
        });
      }
    })
    .catch(err => {
      console.log('regionalSupervisorService', 'Looks like you are not logged in.')
    })
  }

  getDataObservable() {
    return this.dataSource;
  }

  setPage(page) {
    this.page = page;
    this.sendData();
  }


  sendData() {
    this.dataSource.next({
      page: this.page,
      region: this.region,
      user: this.user
    });
  }

  processData() {

    const today = new Date();
    today.setHours(0, 0, 0, 0);
    const tomorrow = new Date();
    tomorrow.setHours(0, 0, 0, 0);
    tomorrow.setDate(tomorrow.getDate() + 1);

    this.region.receives_amount = 0;
    this.region.dispatchs_amount = 0;
    this.region.verified_amount = 0;
    this.region.receives = [];
    this.region.dispatchs = [];
    this.region.active = 0;
    this.region.sales = {
        in: [],
        out: [],
        in_cases: 0,
        out_cases: 0,
      };

    for ( const distributionCenter of this.region.distributionCenters) {

        distributionCenter.receives_amount = 0;
        distributionCenter.active = false;

        for ( const received of distributionCenter.receives) {
          if (!distributionCenter.active && new Date(today) < new Date(received.date)) {
            distributionCenter.active = true;
          }
          distributionCenter.receives_amount += received.amount;
          this.region.receives.push(received);
        }

        this.region.receives_amount += distributionCenter.receives_amount;

        distributionCenter.dispatchs_amount = 0;
        distributionCenter.verified_amount = 0;
        for ( const dispatch of distributionCenter.dispatchs ) {

          if (!distributionCenter.active && new Date(today) < new Date(dispatch.date)) {
            distributionCenter.active = true;
          }

          distributionCenter.dispatchs_amount += dispatch.amount;
          if (dispatch.verified) {
            distributionCenter.verified_amount += dispatch.amount;
          }
          this.region.dispatchs.push(dispatch);
        }

        this.region.dispatchs_amount += distributionCenter.dispatchs_amount;
        this.region.verified_amount += distributionCenter.verified_amount;

        distributionCenter.sale_in_cases = 0;
        for ( const sale_in of distributionCenter.sale_in ) {
          if (!distributionCenter.active && new Date(today) < new Date(sale_in.date)) {
            distributionCenter.active = true;
          }
          if (!sale_in.cases) {
            sale_in.cases = 0;
          }
          distributionCenter.sale_in_cases += parseInt(sale_in.cases);
        }
        this.region.sales.in_cases += distributionCenter.sale_in_cases;


        distributionCenter.sale_out_cases = 0;
        for ( const sale_out of distributionCenter.sale_out ) {
          if (!distributionCenter.active && new Date(today) < new Date(sale_out.date)) {
            distributionCenter.active = true;

          }
          if (!sale_out.cases) {
            sale_out.cases = 0;
          }
          distributionCenter.sale_out_cases += parseInt(sale_out.cases);
        }

        this.region.sales.out_cases += distributionCenter.sale_out_cases;
        if (distributionCenter.active) {
          this.region.active += 1;
        }
      }

    for ( const outlet of this.region.outlets) {
        outlet.receives_amount = 0;
        for ( const received of outlet.receives) {
          outlet.receives_amount += received.amount;
        }
    }
    this.sendData();
  }

}

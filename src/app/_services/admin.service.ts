import { Injectable } from '@angular/core';
import { RestApiService } from './rest-api.service';


declare var $: any;
import * as _moment from 'moment';
// tslint:disable-next-line:no-duplicate-imports
import { default as _rollupMoment, Moment } from 'moment';
import { Subject } from 'rxjs';

const moment = _rollupMoment || _moment;

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  private dataSource = new Subject();
  page: string;
  loadingMessage = 'Data started loading ...';
  nationalReport: any = {
    regions: [],
    outlets: [],
    weeks: [],
    days: [],
    distributionCenters: [],
    endDate: new Date(),
    startDate: new Date(),
    active: 0,
    outlets_active: 0,
    crowns: {
      received: [],
      dispatched: [],
      received_amount: 0,
      dispatched_amount: 0
    },
    sales: {
      in: [],
      out: [],
      in_cases: 0,
      out_cases: 0
    },
    posms: [],
    giveaways: [],
    total_posms: {
      foundPoster: 0,
      newPoster: 0,
      foundTableTalker: 0,
      newTableTalker: 0,
      foundStickerCounter: 0,
      newStickerCounter: 0,
      foundBanners : 0,
      newBanners : 0,
      foundBoardCutOut : 0,
      newBoardCutOut : 0
    },
    total_giveaways: [],
  };


  regions: any[] = [];
  users: any[] = [];
  distributionCenters: any[] = [];
  outlets: any[] = [];
  lidMovements: any[] = [];
  sales: any[] = [];
  dataLoaded: boolean = false;

  constructor(
    private restApi: RestApiService,
  ) {
    this.fetchData();
  }

  getDataObservable() {
    return this.dataSource;
  }

  setPage(page) {
    this.page = page;
    this.sendData();
  }

  sendData() {
    this.dataSource.next({
      page: this.page,
      nationalReport: this.nationalReport,
      loadingMessage: this.loadingMessage,
      dataLoaded: this.dataLoaded
    });
  }

  async fetchData() {

    this.loadingMessage = 'Getting region data';
    this.sendData();

    Promise.all([
      this.getUsers(),
      this.getRegions(),
    ])
    .then((users: any[]) => {


      const regionIds = this.nationalReport.regions.map(region => region._id);


      this.loadingMessage = 'Getting outlet and distribution center data';
      this.sendData();

      Promise.all([
        this.getDistributionCenters(regionIds),
        this.getOutlets(regionIds)
      ])
      .then((dcs: any[]) => {

        this.loadingMessage = 'Getting crown movement and sales data';
        this.sendData();


        const dcIds = this.nationalReport.distributionCenters.map(distributionCenter => distributionCenter._id);
        Promise.all([
          this.getLidMovement(dcIds),
          this.getSales(dcIds),
          this.getGiveaways(),
          this.getPosms()
        ])
        .then(async (sales: any[]) => {

          this.loadingMessage = 'Processing loaded data';
          this.sendData();

          this.nationalReport.outlets_active = 0;
          this.nationalReport.regions = await this.nationalReport.regions.filter(async (region) => {

            region.total_posms = {
              foundPoster: 0,
              newPoster: 0,
              foundTableTalker: 0,
              newTableTalker: 0,
              foundStickerCounter: 0,
              newStickerCounter: 0,
              foundBanners : 0,
              newBanners : 0,
              foundBoardCutOut : 0,
              newBoardCutOut : 0
            };
            // region.total_giveaways = JSON.parse(JSON.stringify(this.nationalReport.products));
            region.total_giveaways = []

            
            // region.terms = this.users.filter(user => user.type == 'regionalSuperviser' && user.region == region._id);
            region.terms = [];
            region.outlets_active = 0;
            region.distributionCenters = await this.nationalReport.distributionCenters.filter(dc => {
              if (dc.region == region._id) {
                dc.terms = [];
                dc.receives = this.nationalReport.crowns.received.filter(lidMovement => lidMovement.distributionCenter._id == dc._id);
                dc.dispatchs = this.nationalReport.crowns.dispatched.filter(lidMovement => lidMovement.distributionCenter._id == dc._id);
                dc.sale_in = this.nationalReport.sales.in.filter(sale => sale.distributionCenter == dc._id);
                dc.sale_out = this.nationalReport.sales.out.filter(sale => sale.distributionCenter == dc._id);
                return dc;
              }
            });

            region.outlets = await this.nationalReport.outlets.filter(async outlet => {
              outlet.receives_amount = 0;

              outlet.receives = [];
              outlet.active = false;
              outlet.receives = await this.nationalReport.crowns.received.filter((lidMovement) =>{
                if (lidMovement.outlet && lidMovement.outlet._id == outlet._id && lidMovement.to == 'distributionCenter' && lidMovement.from == 'outlet') {
                  outlet.receives_amount += lidMovement.amount;
                  outlet.active = true;
                  return lidMovement;
                }
              });

              outlet.total_posms = {
                foundPoster: 0,
                newPoster: 0,
                foundTableTalker: 0,
                newTableTalker: 0,
                foundStickerCounter: 0,
                newStickerCounter: 0,
                foundBanners : 0,
                newBanners : 0,
                foundBoardCutOut : 0,
                newBoardCutOut : 0
              };
              // outlet.total_giveaways = JSON.parse(JSON.stringify(this.nationalReport.products));
              outlet.total_giveaways = []

              outlet.posms = await this.nationalReport.posms.filter(posm => {
                if (posm.outlet == outlet._id) {

                  posm.foundPoster ? outlet.total_posms.foundPoster += parseInt(posm.foundPoster) : 0 ;
                  posm.newPoster ? outlet.total_posms.newPoster += parseInt(posm.newPoster) : 0 ;
                  posm.foundTableTalker ? outlet.total_posms.foundTableTalker += parseInt(posm.foundTableTalker) : 0 ;
                  posm.newTableTalker ? outlet.total_posms.newTableTalker += parseInt(posm.newTableTalker) : 0 ;
                  posm.foundStickerCounter ? outlet.total_posms.foundStickerCounter += parseInt(posm.foundStickerCounter) : 0 ;
                  posm.newStickerCounter ? outlet.total_posms.newStickerCounter += parseInt(posm.newStickerCounter) : 0 ;
                  posm.foundBanners  ? outlet.total_posms.foundBanners += parseInt(posm.foundBanners) : 0 ;
                  posm.newBanners  ? outlet.total_posms.newBanners += parseInt(posm.newBanners) : 0 ;
                  posm.foundBoardCutOut  ? outlet.total_posms.foundBoardCutOut += parseInt(posm.foundBoardCutOut) : 0 ;
                  posm.newBoardCutOut  ? outlet.total_posms.newBoardCutOut  += parseInt(posm.newBoardCutOut) : 0 ;

                  return posm;

                }
              });
             

              region.total_posms.foundPoster += parseInt(outlet.total_posms.foundPoster);
              region.total_posms.newPoster += parseInt(outlet.total_posms.newPoster);
              region.total_posms.foundTableTalker += parseInt(outlet.total_posms.foundTableTalker);
              region.total_posms.newTableTalker += parseInt(outlet.total_posms.newTableTalker);
              region.total_posms.foundStickerCounter += parseInt(outlet.total_posms.foundStickerCounter);
              region.total_posms.newStickerCounter += parseInt(outlet.total_posms.newStickerCounter);
              region.total_posms.foundBanners += parseInt(outlet.total_posms.foundBanners);
              region.total_posms.newBanners += parseInt(outlet.total_posms.newBanners);
              region.total_posms.foundBoardCutOut += parseInt(outlet.total_posms.foundBoardCutOut);
              region.total_posms.newBoardCutOut += parseInt(outlet.total_posms.newBoardCutOut);

              this.nationalReport.total_posms.foundPoster += parseInt(outlet.total_posms.foundPoster);
              this.nationalReport.total_posms.newPoster += parseInt(outlet.total_posms.newPoster);
              this.nationalReport.total_posms.foundTableTalker += parseInt(outlet.total_posms.foundTableTalker);
              this.nationalReport.total_posms.newTableTalker += parseInt(outlet.total_posms.newTableTalker);
              this.nationalReport.total_posms.foundStickerCounter += parseInt(outlet.total_posms.foundStickerCounter);
              this.nationalReport.total_posms.newStickerCounter += parseInt(outlet.total_posms.newStickerCounter);
              this.nationalReport.total_posms.foundBanners += parseInt(outlet.total_posms.foundBanners);
              this.nationalReport.total_posms.newBanners += parseInt(outlet.total_posms.newBanners);
              this.nationalReport.total_posms.foundBoardCutOut += parseInt(outlet.total_posms.foundBoardCutOut);
              this.nationalReport.total_posms.newBoardCutOut += parseInt(outlet.total_posms.newBoardCutOut);


              if (outlet.receives.length) {
                region.outlets_active += 1;
              }
              return outlet;
            }); 
            this.nationalReport.outlets_active += region.outlets_active;


            return region;

          });
          
          this.processData();
          
          
        })
        .catch(err => {
          console.error(err);
        });
      })
      .catch(err => {
        console.error(err);
      });
    })
    .catch(err => {
      console.error(err);
    });
  }

  getRegions() {
    return new Promise((resolve, reject) => {
      this.restApi.getAuth('admin/regions')
      .then((data: any[]) => {
        this.nationalReport.regions = data;
        resolve(data);
      })
      .catch(err => {
        console.log(err);
        reject(err);
      });
    });
  }

  getDistributionCenters(regions: string[]) {
    return new Promise((resolve, reject) => {
    this.restApi.postAuth('admin/distributionCenters', {regions: regions})
    .then((data: any) => {
      this.nationalReport.distributionCenters = data;
      resolve(data);
    })
    .catch(err => {
      console.log(err);
      reject(err);
    });
  });
  }

  getGiveaways() {
    const outletIds = this.nationalReport.outlets.map(outlet => {
      return outlet._id;
    });
    return new Promise((resolve, reject) => {
      this.restApi.postAuth('admin/giveaways', {outlets: outletIds})
      .then((data: any[]) => {
        this.nationalReport.giveaways = data;
        resolve(data);
      })
      .catch(err => {
        console.log(err);
        reject(err);
      });
    });
  }

  getPosms() {
    console.log('getting poms');
    const outletIds = this.nationalReport.outlets.map(outlet => {
      return outlet._id;
    });
    return new Promise((resolve, reject) => {
      this.restApi.postAuth('admin/posms', {outlets: outletIds})
      .then((data: any[]) => {
        this.nationalReport.posms = data;
        resolve(data);
      })
      .catch(err => {
        console.log(err);
        reject(err);
      });
    });
  }

  getOutlets(regions: string[]) {
    return new Promise((resolve, reject) => {
    this.restApi.postAuth('admin/outlets', {regions: regions})
    .then((data: any) => {
      this.nationalReport.outlets = data;
      resolve(data);
    })
    .catch(err => {
      console.log(err);
      reject(err);
    });
  });
  }

  getLidMovement(distributionCenters: string[]) {
    return new Promise((resolve, reject) => {
      this.restApi.postAuth('admin/lidMovements', {distributionCenters: distributionCenters})
      .then((data: any) => {
        this.nationalReport.crowns.received = data.filter(lidMovement => {
          if (lidMovement.from == 'outlet' && lidMovement.to == 'distributionCenter') {
            this.nationalReport.crowns.received_amount += lidMovement.amount;
            return lidMovement;
          }
        });

        this.nationalReport.crowns.dispatched = data.filter(lidMovement => {
          if (lidMovement.from == 'distributionCenter' && lidMovement.to == 'plant') {
            this.nationalReport.crowns.dispatched_amount += lidMovement.amount;
            if  (lidMovement.verified) {
              this.nationalReport.crowns.verified_amount += lidMovement.amount;
            }
            return lidMovement;
          }
        });
        resolve({});
      })
      .catch(err => {
        console.log(err);
        reject(err);
      });
    });
  }

  getSales(distributionCenters: string[]) {
    return new Promise((resolve, reject) => {
      this.restApi.postAuth('admin/sales', {distributionCenters: distributionCenters})
      .then((data: any) => {
        this.nationalReport.sales.in_cases = 0
        this.nationalReport.sales.in = data.filter(sale => {
          if (sale.type == 'in') {
            this.nationalReport.sales.in_cases += parseInt(sale.cases);
            return sale;
          }
        });



        this.nationalReport.sales.out = data.filter(sale => {
          if (sale.type == 'out') {
            this.nationalReport.sales.out_cases += parseInt(sale.cases);
            return sale;
          }
        });
        resolve({});
      })
      .catch(err => {
        console.log(err);
        reject(err);
      });
    });
  }

  getUsers() {
    return new Promise((resolve, reject) => {
      this.restApi.getAuth('admin/users')
      .then((data: any) => {
        this.users = data;
        resolve(data);
      })
      .catch(err => {
        console.log(err);
        reject(err);
      });
    });
  }

  

  processData() {

    this.loadingMessage = 'Finalizing, please wait';
    this.sendData();

    const today = new Date();
    today.setHours(0, 0, 0, 0);
    const tomorrow = new Date();
    tomorrow.setHours(0, 0, 0, 0);
    tomorrow.setDate(tomorrow.getDate() + 1);


    this.nationalReport.weeks = [];
    this.nationalReport.endDate = new Date();
    this.nationalReport.endDate.setDate(this.nationalReport.endDate.getDate() + (1 + 7 - this.nationalReport.endDate.getDay()) % 7);

    if (new Date().getDay() === 0) {
      this.nationalReport.startDate = new Date().setDate(new Date().getDate() - 7);
    } else {
      this.nationalReport.startDate = new Date().setDate(new Date().getDate() - new Date().getDay());
    }


    for (const region of this.nationalReport.regions) {

      

      region.receives_amount = 0;
      region.dispatchs_amount = 0;
      region.verified_amount = 0;
      region.receives = [];
      region.dispatchs = [];
      region.active = 0;
      region.sales = {
        in: [],
        out: [],
        in_cases: 0,
        out_cases: 0,
      };

      if (region.distributionCenters == null) {
        region.distributionCenters = []
      }

      for ( const distributionCenter of region.distributionCenters) {

        distributionCenter.receives_amount = 0;
        distributionCenter.active = false;

        for ( const received of distributionCenter.receives) {
          if (!distributionCenter.active && new Date(today) < new Date(received.date)) {
            distributionCenter.active = true;
          }
          new Date(this.nationalReport.startDate) > new Date(received.date) ? this.nationalReport.startDate = new Date(received.date) :  false ;
          distributionCenter.receives_amount += received.amount;
          region.receives.push(received);
        }
        region.receives_amount += distributionCenter.receives_amount;

        distributionCenter.dispatchs_amount = 0;
        distributionCenter.verified_amount = 0;
        for ( const dispatch of distributionCenter.dispatchs ) {

          if (!distributionCenter.active && new Date(today) < new Date(dispatch.date)) {
            distributionCenter.active = true;
          }

          // tslint:disable-next-line: max-line-length
          new Date(this.nationalReport.startDate) > new Date(dispatch.date) ? this.nationalReport.startDate = new Date(dispatch.date) : false ;
          distributionCenter.dispatchs_amount += dispatch.amount;
          if (dispatch.verified) {
            distributionCenter.verified_amount += dispatch.amount;
          }
          region.dispatchs.push(dispatch);
        }

        region.dispatchs_amount += distributionCenter.dispatchs_amount;
        region.verified_amount += distributionCenter.verified_amount;

        distributionCenter.sale_in_cases = 0;
        for ( const sale_in of distributionCenter.sale_in ) {

          if (!distributionCenter.active && new Date(today) < new Date(sale_in.date)) {
            distributionCenter.active = true;
          }
          if (!sale_in.cases) {
            sale_in.cases = 0;
          }
          distributionCenter.sale_in_cases += parseInt(sale_in.cases);
          // tslint:disable-next-line: max-line-length
          new Date(this.nationalReport.startDate) > new Date(sale_in.date) ? this.nationalReport.startDate = new Date(sale_in.date) : false ;
        }
        region.sales.in_cases += distributionCenter.sale_in_cases;
        distributionCenter.sale_out_cases = 0;
        for ( const sale_out of distributionCenter.sale_out ) {
          if (!distributionCenter.active && new Date(today) < new Date(sale_out.date)) {
            distributionCenter.active = true;
          }
          if (!sale_out.cases) {
            sale_out.cases = 0;
          }
          distributionCenter.sale_out_cases += parseInt(sale_out.cases);
          // tslint:disable-next-line: max-line-length
          new Date(this.nationalReport.startDate) > new Date(sale_out.date) ? this.nationalReport.startDate = new Date(sale_out.date) : false ;

        }
        region.sales.out_cases += distributionCenter.sale_out_cases;
        if (distributionCenter.active) {
          region.active += 1;
          this.nationalReport.active += 1;
        }
      }
    }

    
    this.nationalReport.startDate = this.getMonday(this.nationalReport.startDate);

    let number_of_weeks = Math.floor(((this.nationalReport.endDate - this.nationalReport.startDate) / (1000 * 60 * 60 * 24)) / 7);
    let number_of_days = Math.floor(((this.nationalReport.endDate - this.nationalReport.startDate) / (1000 * 60 * 60 * 24)));
    if (number_of_weeks > 25 ) {
      number_of_weeks = 25;
    }

    this.nationalReport.weeks = [];
    for (let index = 0; index < number_of_weeks; index++) {

      const startdate = new Date(this.nationalReport.startDate);
      startdate.setDate(startdate.getDate() + (index * 7));

      const enddate = new Date(this.nationalReport.startDate);
      enddate.setDate(enddate.getDate() + ((index + 1) * 7));

      const week = {
        name: index + 1,
        from: startdate,
        to: enddate,
        received : [],
        received_amount : 0,
        dispatched : [],
        dispatched_amount : 0,
        sale_in : [],
        sale_in_amount : 0,
        sale_out : [],
        sale_out_amount : 0,
        
      };


      for (const received of this.nationalReport.crowns.received) {
        if (new Date(received.date) >= new Date(week.from) && new Date(received.date) <= new Date(week.to)) {
          week.received.push(received);
          // tslint:disable-next-line: radix
          week.received_amount += parseInt(received.amount);
        }
      }

      for (const dispatched of this.nationalReport.crowns.dispatched) {
        if (new Date(dispatched.date) >= new Date(week.from) && new Date(dispatched.date) <= new Date(week.to)) {
          week.dispatched.push(dispatched);
          week.dispatched_amount += parseInt(dispatched.amount);
        }
      }

      for (const sale_in of this.nationalReport.sales.in) {
        if (new Date(sale_in.date) >= new Date(week.from) && new Date(sale_in.date) <= new Date(week.to)) {
          week.sale_in.push(sale_in);
          week.sale_in_amount += parseInt(sale_in.cases);
        }
      }

      for (const sale_out of this.nationalReport.sales.out) {
        if (new Date(sale_out.date) >= new Date(week.from) && new Date(sale_out.date) <= new Date(week.to)) {
          week.sale_out.push(sale_out);
          week.sale_out_amount += parseInt(sale_out.cases);
        }
      }

      
      this.nationalReport.weeks.push(week);

    }

    this.nationalReport.days = []
    for (let index = 0; index < number_of_days; index++) {

      const startdate = new Date(this.nationalReport.startDate);
      startdate.setDate(startdate.getDate() + (index));

      const enddate = new Date(this.nationalReport.startDate);
      enddate.setDate(enddate.getDate() + ((index + 1)));

      let day = {
        from: startdate,
        to: enddate,
        date: startdate,
        posms: [],
        outlets: [],
        poster: 0,
        tableTalker: 0,
        stickerCounter: 0,
        banners : 0,
        boardCutOut : 0,
      }

      for (const posm of this.nationalReport.posms) {
        if (new Date(posm.date) >= new Date(day.from) && new Date(posm.date) <= new Date(day.to)) {
          day.posms.push(posm);

          let outlet = this.nationalReport.outlets.filter(outlet => outlet._id == posm.outlet)[0]
          if(day.outlets.indexOf(outlet) == -1) {

            day.outlets.push(outlet);
            day.poster += posm.foundPoster > 0 || posm.newPoster > 0 ? 1 : 0;
            day.tableTalker += posm.foundTableTalker > 0 || posm.newTableTalker > 0 ? 1 : 0;
            day.stickerCounter += posm.foundStickerCounter > 0 || posm.newStickerCounter > 0 ? 1 : 0;
            day.banners  += posm.foundBanners > 0 || posm.newBanners > 0 ? 1 : 0;
            day.boardCutOut  += posm.foundBoardCutOut > 0 || posm.newBoardCutOut > 0 ? 1 : 0;  

          }

        }
      }
      this.nationalReport.days.push(day);
    }
    this.loadingMessage = 'Data Loading successful';
    this.dataLoaded = true;
    this.sendData();

  }

  getMonday(d) {
    d = new Date(d);
    const day = d.getDay();
    const diff = d.getDate() - day + (day === 0 ? -6 : 0); // adjust when day is sunday
    return new Date(d.setDate(diff));
  }
}

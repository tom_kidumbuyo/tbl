import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegionalSupervisorComponent } from './regional-supervisor.component';

describe('RegionalSupervisorComponent', () => {
  let component: RegionalSupervisorComponent;
  let fixture: ComponentFixture<RegionalSupervisorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegionalSupervisorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegionalSupervisorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

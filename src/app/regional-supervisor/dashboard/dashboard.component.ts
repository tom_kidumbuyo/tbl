import { Component, OnInit } from '@angular/core';
import { RegionalSupervisorService } from 'src/app/_services/regional-supervisor.service';
import { AuthenticationService } from 'src/app/_services/authentication.service';
import { Router } from '@angular/router';
import { RestApiService } from 'src/app/_services/rest-api.service';
import { FormBuilder } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  regionalSupervisorDataObserver: any;
  region: any = {
    name: 'Loading ...',
    receives_amount: 0,
    dispatchs_amount: 0,
    verified_amount: 0,
    receives: [],
    dispatchs: [],
    active: 0,
    sales: {
        in: [],
        out: [],
        in_cases: 0,
        out_cases: 0,
    },
  };

  d = new Date();
  hh = ('0' + Math.abs((this.d.getTimezoneOffset() - (this.d.getTimezoneOffset() % 60)) / 60)).slice(-2);
  mm = ('0' +  Math.abs(this.d.getTimezoneOffset() % 60)).slice(-2);
  tz = this.d.getTimezoneOffset() > 0 ? '-' + this.hh + this.mm : '+' + this.hh + this.mm;

  constructor(
    private regionalSupervisorService: RegionalSupervisorService,
    private restApi: RestApiService,
    private snackBar: MatSnackBar,
  ) {

    this.regionalSupervisorDataObserver = this.regionalSupervisorService.getDataObservable();
    this.regionalSupervisorDataObserver.subscribe(data => {
      if (data.region) {
        this.region = data.region;
        console.log('dashboard region', this.region);
      }
    });
    this.regionalSupervisorService.setPage('dashboard');
  }

  ngOnInit() {
  }

}

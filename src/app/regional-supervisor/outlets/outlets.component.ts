import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/_services/authentication.service';
import { Router } from '@angular/router';
import { RestApiService } from 'src/app/_services/rest-api.service';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { RegionalSupervisorService } from 'src/app/_services/regional-supervisor.service';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmationDialogComponent } from 'src/app/shared/confirmation-dialog/confirmation-dialog.component';

declare var $: any;

@Component({
  selector: 'app-outlets',
  templateUrl: './outlets.component.html',
  styleUrls: ['./outlets.component.scss']
})
export class OutletsComponent implements OnInit {

  outlets: any = [];
  regionalSupervisorDataObserver: any;
  newOutletForm: any;
  region: any = {
    name: 'Loading ...'
  };
  selectedOutlet: any = false;

  constructor(
    private regionalSupervisorService: RegionalSupervisorService,
    private restApi: RestApiService,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private dialog: MatDialog
  ) {

    this.regionalSupervisorDataObserver = this.regionalSupervisorService.getDataObservable();
    this.regionalSupervisorDataObserver.subscribe(data => {
      if (data.region) {

        this.region = data.region;
        console.log('region', this.region);
        this.restApi.getAuth('region/' + this.region._id + '/outlets')
        .then((outlets: any) => {
          this.outlets = outlets;
        })
        .catch(err => {

          this.snackBar.open(err.error.message, 'Close', {
            verticalPosition: 'top'
          });
        });
      }
    });
    this.regionalSupervisorService.setPage('outlets');



    this.newOutletForm = this.formBuilder.group({
      name: new FormControl('', Validators.required),
      first_name: new FormControl('', Validators.required),
      last_name: new FormControl('', Validators.required),
      phone: new FormControl('', Validators.required),
      region: new FormControl('', Validators.required),
      district: new FormControl('', Validators.required),
      town: new FormControl('', Validators.required)
    });


  }

  ngOnInit() {
  }

  createOutlet() {

    const outletData = this.newOutletForm.value;
    outletData.region = this.region._id;

    if (this.selectedOutlet) {
      this.restApi.putAuth('outlet/' + this.selectedOutlet._id, outletData)
      .then((data: any) => {
        this.outlets[this.outlets.indexOf(this.selectedOutlet)] = data;
        $('#new-outlet').modal('hide');
      })
      .catch((err) => {
        console.log(err);
        this.snackBar.open(err.error.message, 'Close', {
          verticalPosition: 'top'
        });
      });
    } else {

      this.restApi.postAuth('outlet/create', outletData)
      .then((data: any) => {
        this.outlets.push(data);
        $('#new-outlet').modal('hide');
      })
      .catch((err) => {
        console.log(err);
        this.snackBar.open(err.error.message, 'Close', {
          verticalPosition: 'top'
        });
      });
    }
  }

  deleteOutlet(outlet) {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: 'Do you confirm the deletion of this user.'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.restApi.deleteAuth('outlet/' + outlet._id)
        .then(data => {
          this.outlets.splice(this.outlets.indexOf(outlet), 1);
          this.snackBar.open('Outlet successfully deleted.', 'Close', {
            verticalPosition: 'top'
          });
        })
        .catch(err => {
          this.snackBar.open('Error deleting user', 'Close', {
            verticalPosition: 'top'
          });
        });
      }
    });
  }



  editOutlet(outlet) {
    console.log(outlet);
    this.selectedOutlet = outlet;
    if (outlet) {
      const owner = outlet.owner.split(' ');
      this.newOutletForm.setValue({
        region: this.region._id,
        name: outlet.name,
        first_name: owner[0],
        last_name: owner[1],
        phone: outlet.phone,
        district: outlet.district._id,
        town: outlet.town
      });
    }
  }

}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../_services/authentication.service';
import { RegionalSupervisorService } from '../_services/regional-supervisor.service';

@Component({
  selector: 'app-regional-supervisor',
  templateUrl: './regional-supervisor.component.html',
  styleUrls: ['./regional-supervisor.component.scss']
})
export class RegionalSupervisorComponent implements OnInit {

  regionalSupervisorDataObserver: any;
  page: any;

  constructor(
    private regionalSupervisorService: RegionalSupervisorService,
    private auth: AuthenticationService,
    private router: Router
  ) {

    this.regionalSupervisorDataObserver = this.regionalSupervisorService.getDataObservable();
    this.regionalSupervisorDataObserver.subscribe(data => {
      this.page = data.page;
    });

  }

  ngOnInit() {
    this.auth.isLoggedIn()
    .then((data: any) => {
      if (data.type == 'admin') {
        this.router.navigate(['/admin']);
      } else if (data.type == 'distributionCenter')  {
        this.router.navigate(['/distributionCenter']);
      } else if (data.type == 'aim')  {
        this.router.navigate(['/aim']);
      } else if (data.type == 'plant')  {
        this.router.navigate(['/plant']);
      }
    })
    .catch(err => {
      this.router.navigate(['/auth']);
    });
  }

}

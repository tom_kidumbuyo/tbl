import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegionalSupervisorRoutingModule } from './regional-supervisor-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DistributionCenterComponent } from './distribution-center/distribution-center.component';
import { OutletsComponent } from './outlets/outlets.component';
import { SharedModule } from '../shared/shared.module';
import { RegionalSupervisorComponent } from './regional-supervisor.component';


@NgModule({
  declarations: [DashboardComponent, DistributionCenterComponent, OutletsComponent, RegionalSupervisorComponent],
  imports: [
    CommonModule,
    RegionalSupervisorRoutingModule,
    SharedModule
  ]
})
export class RegionalSupervisorModule { }

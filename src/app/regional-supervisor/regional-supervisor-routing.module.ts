import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegionalSupervisorComponent } from './regional-supervisor.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DistributionCenterComponent } from './distribution-center/distribution-center.component';
import { OutletsComponent } from './outlets/outlets.component';




const routes: Routes = [{
  path: 'regionalSupervisor',
  component: RegionalSupervisorComponent,
  children: [{
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  }, {
    path: 'distributionCenters',
    component: DistributionCenterComponent
  }, {
    path: 'dashboard',
    component: DashboardComponent
  }, {
    path: 'outlets',
    component: OutletsComponent
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegionalSupervisorRoutingModule { }

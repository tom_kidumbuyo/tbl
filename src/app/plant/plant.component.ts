import { Component, OnInit } from '@angular/core';
import { RestApiService } from '../_services/rest-api.service';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from '../_services/authentication.service';
import { MatSnackBar } from '@angular/material/snack-bar';

declare var $: any;

@Component({
  selector: 'app-plant',
  templateUrl: './plant.component.html',
  styleUrls: ['./plant.component.scss']
})
export class PlantComponent implements OnInit {

  dispatchs: any;
  selectedDispatch: any = {};
  dispatch_total: number;
  boxes = new FormArray([]);

  d = new Date();
  hh = ('0' + Math.abs((this.d.getTimezoneOffset() - (this.d.getTimezoneOffset() % 60)) / 60)).slice(-2);
  mm = ('0' +  Math.abs(this.d.getTimezoneOffset() % 60)).slice(-2);
  tz = this.d.getTimezoneOffset() > 0 ? '-' + this.hh + this.mm : '+' + this.hh + this.mm;

  constructor(
    private restApi: RestApiService,
    private router: Router,
    private auth: AuthenticationService,
    private snackBar: MatSnackBar,
  ) {

    this.auth.isLoggedIn()
    .then((data: any) => {
      if (data.type == 'regionalsupervisor') {
        this.router.navigate(['/regionalsupervisor']);
      } else if (data.type == 'distributionCenter')  {
        this.router.navigate(['/distributionCenter']);
      } else if (data.type == 'aim')  {
        this.router.navigate(['/aim']);
      } else if (data.type == 'admin')  {
        this.router.navigate(['/admin']);
      }
    })
    .catch(err => {
      this.router.navigate(['/auth']);
    });

    this.restApi.getAuth('plant/dispatchs')
    .then((data: any) => {
      this.dispatchs = data;
    })
    .catch(err => {
      console.log(err);
    });
  }

  ngOnInit() {
  }

  edit(dispatch) {

    this.selectedDispatch = dispatch;
    while (this.boxes.length !== 0) {
      this.boxes.removeAt(0);
    }

    for (const bx of dispatch.boxes) {
        const box = new FormGroup({
          number: new FormControl(bx.number),
          amount: new FormControl(0)
        });
        this.boxes.push(box);
    }
    this.refreshBoxTotal();
  }

  return(dispatch) {
    this.restApi.getAuth('admin/returnDispatch/' + dispatch._id)
    .then((data: any) => {
      this.dispatchs[this.dispatchs.indexOf(dispatch)] = data;
    })
    .catch(err => {
      console.log(err);
    });
  }

  refreshBoxTotal() {
    this.dispatch_total = 0;
    for (const box of this.boxes.controls) {
      console.log(box);
      this.dispatch_total += box.value.amount;
    }
  }

  verifyDispatch(dispatch) {
    for (const box of this.boxes.controls) {
      if (isNaN(box.value.amount) || box.value.amount <= 0) {
        this.snackBar.open("Please fill all the boxes with a number greater that zero.", 'Close', {
          verticalPosition: 'top'
        });
        return;
      }
    }
    this.restApi.get('admin/verifyDispatch/' + dispatch._id)
    .then((data: any) => {
        this.dispatchs[this.dispatchs.indexOf(dispatch)] = data;
    })
    .catch(err => {
        console.log(err);
    });

  }

  cancelDispatch(dispatch) {
    this.restApi.getAuth('admin/cancelDispatch/' + dispatch._id)
    .then((data: any) => {
      this.dispatchs[this.dispatchs.indexOf(dispatch)] = data;
    })
    .catch(err => {
      console.log(err);
    });
  }

  save() {

    console.log(this.boxes.value);
    this.restApi.postAuth('plant/verifyDispatch/' + this.selectedDispatch._id, {boxes : this.boxes.value })
    .then((data: any) => {
      this.dispatchs[this.dispatchs.indexOf(this.selectedDispatch)] = data;
      $('#dispatch-modal').modal('hide');
    })
    .catch(err => {
      console.log(err);
    });
  }
}

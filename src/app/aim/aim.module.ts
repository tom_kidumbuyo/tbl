import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AimRoutingModule } from './aim-routing.module';
import { AimComponent } from './aim.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [AimComponent],
  imports: [
    CommonModule,
    AimRoutingModule,
    SharedModule
  ]
})
export class AimModule { }

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AimComponent } from './aim.component';


const routes: Routes = [{
  path: 'aim',
  component: AimComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AimRoutingModule { }

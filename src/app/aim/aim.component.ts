import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormBuilder } from '@angular/forms';
import { AuthenticationService } from '../_services/authentication.service';
import { Router } from '@angular/router';
import { RestApiService } from '../_services/rest-api.service';

declare var $: any;

@Component({
  selector: 'app-aim',
  templateUrl: './aim.component.html',
  styleUrls: ['./aim.component.scss']
})
export class AimComponent implements OnInit {

  socialMediaForm: any;
  selectedComment: any;
  comments: any[] = [];
  maxDate: Date;

  d = new Date();
  hh = ('0' + Math.abs((this.d.getTimezoneOffset() - (this.d.getTimezoneOffset() % 60)) / 60)).slice(-2);
  mm = ('0' +  Math.abs(this.d.getTimezoneOffset() % 60)).slice(-2);
  tz = this.d.getTimezoneOffset() > 0 ? '-' + this.hh + this.mm : '+' + this.hh + this.mm;

  constructor(
    private auth: AuthenticationService,
    private router: Router,
    private restApi: RestApiService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {

    this.auth.isLoggedIn()
    .then((data: any) => {
      if (data.type == 'regionalsupervisor') {
        this.router.navigate(['/regionalsupervisor']);
      } else if (data.type == 'distributionCenter')  {
        this.router.navigate(['/distributionCenter']);
      } else if (data.type == 'admin')  {
        this.router.navigate(['/admin']);
      } else if (data.type == 'plant')  {
        this.router.navigate(['/plant']);
      }
    })
    .catch(err => {
      this.router.navigate(['/auth']);
    });

    this.maxDate = new Date();

    this.socialMediaForm = this.formBuilder.group({
      id:  new FormControl(false, Validators.required),
      platform: new FormControl('facebook', Validators.required),
      positive: new FormControl(0, Validators.required),
      negative: new FormControl(0, Validators.required),
      neutral: new FormControl(0, Validators.required),
      date: new FormControl(new Date(), Validators.required),
    });

    this.restApi.getAuth('comment')
    .then((data: any[]) => {
      console.log(data);
      this.comments = data;
    })
    .catch(err => {
      console.log(err)
    });

  }

  edit(comment) {
    this.selectedComment = comment;
    this.socialMediaForm.setValue({
      id: comment._id,
      platform: comment.platform,
      positive: comment.positive,
      negative: comment.negative,
      date: new Date(comment.date),
    });
  }

  delete(comment) {
    this.restApi.deleteAuth('comment/' + comment._id)
    .then(data => {
      this.comments.splice(this.comments.indexOf(comment), 1);
    })
    .catch(err => {
      console.log(err)
    });
  }

  clearComment() {
    this.selectedComment = false;
    this.socialMediaForm.setValue({
      id: false,
      platform: 'facebook',
      positive: 0,
      negative: 0,
      date: new Date()
    });

  }

  save() {

    if (this.socialMediaForm.value.id) {

      this.restApi.putAuth('comment/' + this.socialMediaForm.value.id, this.socialMediaForm.value)
      .then(data => {
        this.selectedComment = data;
        $('#comment-modal').modal('hide');
      })
      .catch(err => {
        console.log(err);
      });

    } else {
      this.restApi.postAuth('comment/create/', this.socialMediaForm.value)
      .then(data => {
        this.comments.push(data);
        $('#comment-modal').modal('hide');
      })
      .catch(err => {
        console.log(err);
      });

    }
  }

}

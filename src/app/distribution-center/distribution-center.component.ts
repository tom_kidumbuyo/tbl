import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../_services/authentication.service';
import { Router } from '@angular/router';
import { RestApiService } from '../_services/rest-api.service';
import { FormControl, FormBuilder, Validators, FormArray, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ConfirmationDialogComponent } from 'src/app/shared/confirmation-dialog/confirmation-dialog.component';
import { MatDialog } from '@angular/material/dialog';

declare var $: any;

@Component({
  selector: 'app-distribution-center',
  templateUrl: './distribution-center.component.html',
  styleUrls: ['./distribution-center.component.scss']
})
export class DistributionCenterComponent implements OnInit {

  user: any;
  distributionCenter: any;
  region: any;

  distributionCenters: any = [];
  regions: any = [];

  receivedLids: any[];
  dispatchedLids: any[];
  outlets: any = [];

  cratePrice = 25000;
  lidPrice = 200;
  lidNumberDividend: number;

  today = new Date();

  d = new Date();
  hh = ('0' + Math.abs((this.d.getTimezoneOffset() - (this.d.getTimezoneOffset() % 60)) / 60)).slice(-2);
  mm = ('0' +  Math.abs(this.d.getTimezoneOffset() % 60)).slice(-2);
  tz = this.d.getTimezoneOffset() > 0 ? '-' + this.hh + this.mm : '+' + this.hh + this.mm ;

  regionModel: String ;

  ReceiveForm = new FormGroup({
    from: new FormControl('outlet', Validators.required),
    to: new FormControl('distributionCenter', Validators.required),
    distributionCenter: new FormControl('', Validators.required),
    outlet: new FormControl('', Validators.required),
    amount: new FormControl('', Validators.required),
    status: new FormControl('received', Validators.required),
    date: new FormControl(this.today.toDateString(), Validators.required),
  });

  DispatchForm = new FormGroup({
    from: new FormControl('distributionCenter', Validators.required),
    to: new FormControl('plant', Validators.required),
    distributionCenter: new FormControl('', Validators.required),
    amount: new FormControl('', Validators.required),
    vehicle_number: new FormControl('', Validators.required),
    status: new FormControl('dispatched', Validators.required),
    boxes: new FormArray([]),
    date: new FormControl(this.today.toDateString(), Validators.required),
  });

  SellInForm = new FormGroup({
    cases: new FormControl(0, Validators.required),
    distributionCenter: new FormControl('', Validators.required),
    date: new FormControl(this.today.toDateString(), Validators.required),
  });

  SellOutForm = new FormGroup({
    cases: new FormControl(0, Validators.required),
    distributionCenter: new FormControl('', Validators.required),
    date: new FormControl(this.today.toDateString(), Validators.required),
  });

  boxes: FormArray = this.DispatchForm.get('boxes') as FormArray;
  selectedPage: any;
  dispatch_total: number;
  sales: {in: any[], out: any[]} = {in: [], out: []};

  regionalSupervisorDataObserver: any;
  newOutletForm: any;
  selectedOutlet: any = false;
  latlng = {lng: 0, lat: 0};


  districts = [];
  wards = [];
  allDistricts: any = [];
  allWards: any = [];
  PosmForm: FormGroup;
  posms: any[] = [];

  constructor(
    private auth: AuthenticationService,
    private router: Router,
    private restApi: RestApiService,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    public dialog: MatDialog
  ) {

    console.log('timezone', this.tz);

    this.lidNumberDividend = 162;
    this.selectPage('received');
    this.addBox();

    

  }

  ngOnInit() {
    this.auth.isLoggedIn()
    .then((data: any) => {

      this.user = data;
      console.log(data);

      if (data.type === 'regionalSuperviser') {
        this.router.navigate(['/regionalSupervisor']);
      } else if (data.type === 'admin')  {
        this.router.navigate(['/admin']);
      } else if (data.type == 'aim')  {
        this.router.navigate(['/aim']);
      } else if (data.type == 'plant')  {
        this.router.navigate(['/plant']);
      }

      this.restApi.get('distributionCenter/lids')
      .then((lids: any) => {
        this.receivedLids = lids.received;
        this.dispatchedLids = lids.dispatched;
      })
      .catch(err => {
        console.error(err);
      });

      this.restApi.getAuth('distributionCenter/sales')
      .then((data: any) => {
        this.sales = data;
      });


    })
    .catch(err => {
      this.router.navigate(['/auth']);
    });


    this.newOutletForm = this.formBuilder.group({
      name: new FormControl('', Validators.required),
      first_name: new FormControl('', Validators.required),
      last_name: new FormControl('', Validators.required),
      phone: new FormControl('', Validators.required),
      latitude: new FormControl(0, Validators.required),
      longitude: new FormControl(0, Validators.required),
      region: new FormControl(null, Validators.required),
      district: new FormControl(null, Validators.required),
      ward: new FormControl(null, Validators.required),
      date: new FormControl(this.today.toDateString(), Validators.required),
    });

    this.PosmForm = new FormGroup({
      foundPoster: new FormControl(0, Validators.required),
      newPoster: new FormControl(0, Validators.required),
      foundTableTalker: new FormControl(0, Validators.required),
      newTableTalker: new FormControl(0, Validators.required),
      foundStickerCounter: new FormControl(0, Validators.required),
      newStickerCounter: new FormControl(0, Validators.required),
      foundBanners: new FormControl(0, Validators.required),
      newBanners: new FormControl(0, Validators.required),
      foundBoardCutOut: new FormControl(0, Validators.required),
      newBoardCutOut: new FormControl(0, Validators.required),
      date: new FormControl(this.today.toDateString(), Validators.required),
    });

    Promise.all([this.restApi.getAuth('location/districts'), this.restApi.getAuth('location/wards'), this.restApi.getAuth('region')]).then((values) => {
      console.log("REGIONS", values[2]);
      console.log('districts', values[0]);
      console.log('wards', values[1]);
      this.allDistricts = values[0];
      this.allWards = values[1];
      this.regions = values[2];
      this.regionModel = this.regions[0]._id
      this.regionSelected();
    })
    .catch(err => {

      console.error(err);
      this.snackBar.open('error loading location data', 'Close', {
        verticalPosition: 'top'
      });

    });

    this.auth.isLoggedIn()
    .then(data => {
      if (data) {
        this.user = data;
      }
    });

    this.restApi.getAuth('outlet')
      .then((outlets: any[]) => {
        this.outlets = outlets;
        console.log('outlets new ', this.outlets);
      })
      .catch(err => {
        this.snackBar.open('error loading outlets', 'Close', {
          verticalPosition: 'top'
        });
      });

     

  }

  selectOutlet(outlet) {
    this.selectedOutlet = outlet;
    if(outlet != null) {
      this.restApi.getAuth('outlet/' + outlet._id + '/posms')
      .then((posms: any) => {
          this.posms = posms;
      })
      .catch(err => {
        this.snackBar.open('error loading outlet', 'Close', {
          verticalPosition: 'top'
        });
      });
    }
  }

  selectRegion() {

    this.distributionCenters = [];
    this.restApi.getAuth('region/' + this.regionModel + '/distributionCenters')
    .then((data) => {
      this.distributionCenters = data;
    })
    .catch((err) => {
      console.log(err);
      this.snackBar.open(err.error.message, 'Close', {
        verticalPosition: 'top'
      });
    });

  }

  receive() {

    console.log(this.ReceiveForm.value);

    if (!this.ReceiveForm.value.amount) {
      this.snackBar.open('The received amount must greater than zero', 'Close', {
        verticalPosition: 'top'
      });
    } else if (!this.ReceiveForm.value.distributionCenter) {
      this.snackBar.open('Please select a distribution center', 'Close', {
        verticalPosition: 'top'
      });
    }else {
      this.lidMovement(this.ReceiveForm.value)
      .then((data: any) => {
        this.receivedLids.push(data);
        this.clearForms();
      })
      .catch((err) => {
        console.log(err);
        this.snackBar.open(err.error.message, 'Close', {
          verticalPosition: 'top'
        });
      });
    }
  }

  dispatch() {
    if (!this.dispatch_total) {
      this.snackBar.open('The total amount must greater than zero', 'Close', {
        verticalPosition: 'top'
      });
    } else {
      for (const box of this.boxes.controls) {
        if (box.value.amount < 0) {
          alert('Please do not add empty boxes.');
          return;
        }
        if (box.value.number == '') {
          alert('Please add box numbers to all boxes.');
          return;
        }
      }

      console.log(this.DispatchForm.value);

      this.lidMovement(this.DispatchForm.value)
      .then((data: any) => {
          this.dispatchedLids.push(data);
          this.clearForms();
      })
      .catch((err) => {
          console.log(err);
          this.snackBar.open(err.error.message, 'Close', {
            verticalPosition: 'top'
          });
      });
    }
  }

  clearForms() {

    $('#dispatch-modal').modal('hide');
    $('#receive-modal').modal('hide');
    $('#sale-in-modal').modal('hide');
    $('#sale-out-modal').modal('hide');

    this.ReceiveForm = new FormGroup({
      from: new FormControl('outlet', Validators.required),
      to: new FormControl('distributionCenter', Validators.required),
      distributionCenter: new FormControl('', Validators.required),
      amount: new FormControl('', Validators.required),
      status: new FormControl('received', Validators.required),
    });

    this.DispatchForm = new FormGroup({
      from: new FormControl('distributionCenter', Validators.required),
      to: new FormControl('plant', Validators.required),
      distributionCenter: new FormControl('', Validators.required),
      amount: new FormControl('', Validators.required),
      vehicle_number: new FormControl('', Validators.required),
      status: new FormControl('dispatched', Validators.required),
      boxes: new FormArray([])
    });

    this.SellInForm = new FormGroup({
      cases: new FormControl(0, Validators.required),
    });

    this.SellOutForm = new FormGroup({
      cases: new FormControl(0, Validators.required),
      outlet: new FormControl('', Validators.required),
    });

  }

  refreshTotals() {

  }

  addBox() {
    const box = new FormGroup({
      number: new FormControl(''),
      amount: new FormControl(0)
    });
    this.boxes.push(box);
  }

  removeBox(i) {
    this.boxes.removeAt(i);
  }

  refreshBoxTotal() {

    this.dispatch_total = 0;
    for (const box of this.boxes.controls) {
      console.log('box', box);
      this.dispatch_total += box.value.amount;
    }
    this.DispatchForm.controls.amount.setValue(this.dispatch_total);
    console.log('form', this.DispatchForm.value);
  }

  selectPage(page) {
    this.selectedPage = page;
  }

  lidMovement(postData) {

    // postData.distributionCenter = this.distributionCenter._id;
    return new Promise((resolve, reject) => {
      this.restApi.postAuth('lidsMovement/create', postData)
      .then((data: any) => {
        resolve(data);
      })
      .catch(err => {
        reject(err);
      });
    });
  }

  deleteLidMovement(lid) {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: 'Do you confirm the deletion of this lid Movement?.'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.restApi.deleteAuth('lidsMovement/' + lid._id)
        .then(data => {
          this.snackBar.open('Crown Movement deleted successfully', 'Close', {
            verticalPosition: 'top'
          });
          if (this.dispatchedLids.indexOf(lid)) {
            this.dispatchedLids.splice(this.dispatchedLids.indexOf(lid), 1);
          } else {
            this.receivedLids.splice(this.receivedLids.indexOf(lid), 1);
          }

          this.restApi.get('distributionCenter/' + this.user.dc + '/lids')
          .then((lids: any) => {
            this.receivedLids = lids.received;
            this.dispatchedLids = lids.dispatched;
          })
          .catch(err => {
            console.error(err);
          });
        })
        .catch(err => {
          this.snackBar.open('Error deleting Crown Movement.', 'Close', {
            verticalPosition: 'top'
          });
        });
      }
    });
  }

  saleIn() {
    if (isNaN(this.SellInForm.value.cases) || this.SellInForm.value.cases === null) {
      this.snackBar.open('Please fill in the amount.', 'Close', {
        verticalPosition: 'top'
      });
    } else {
      const postData = this.SellInForm.value;
      this.restApi.postAuth('sales/in/create', postData)
      .then((data: any) => {
        this.sales.in.push(data);
        this.clearForms();
        $('#sale-in-modal').modal('close');
      })
      .catch((err: any) => {
        console.log(err);
        this.snackBar.open(err.error.message, 'Close', {
          verticalPosition: 'top'
        });
      });
    }
  }

  saleOut() {
    console.log(this.SellOutForm.value);
    if (isNaN(this.SellOutForm.value.cases) || this.SellOutForm.value.cases === null) {
      this.snackBar.open('Please fill in the amount.', 'Close', {
        verticalPosition: 'top'
      });
    } else {
      const postData = this.SellOutForm.value;

      this.restApi.postAuth('sales/out/create', postData)
      .then((data: any) => {
        this.sales.out.push(data);
        this.clearForms();
      })
      .catch((err: any) => {
        console.log(err);
        this.snackBar.open(err.error.message, 'Close', {
          verticalPosition: 'top'
        });
      });
    }
  }

  deleteSale(sale) {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: 'Do you confirm the deletion of this Sale?.'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.restApi.deleteAuth('sales/' + sale._id)
        .then((data: any) => {
          if (sale.type == 'in') {
            this.sales.in.splice(this.sales.in.indexOf(sale), 1);
          } else {
            this.sales.out.splice(this.sales.out.indexOf(sale), 1);
          }
        })
        .catch((err: any) => {
          console.log(err);
          this.snackBar.open('Error deleting the sale', 'Close', {
            verticalPosition: 'top'
          });
        });
      }
    });
  }

  regionSelected() {
    console.log(this.newOutletForm);
    this.districts = this.allDistricts.filter(district => district.region === this.newOutletForm.value.region);
    this.newOutletForm.controls.district.setValue(this.districts[0]._id);
    this.districtSelected();
  }

  districtSelected() {
    console.log(this.newOutletForm.value);
    this.wards = this.allWards.filter(ward => ward.district == this.newOutletForm.value.district);
    console.log(this.wards[0]);
    this.newOutletForm.controls.ward.setValue(this.wards[0]._id);
  }

  getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition((position)=>{
          const coords = position.coords;
          this.latlng.lat = coords.latitude;
          this.latlng.lng = coords.longitude;
        });
    }
  }

  createOutlet() {

    const outletData = this.newOutletForm.value;
    outletData.latlng = this.latlng;

    
      if (this.selectedOutlet) {
        this.restApi.putAuth('outlet/' + this.selectedOutlet._id, outletData)
        .then((data: any) => {
          this.outlets[this.outlets.indexOf(this.selectedOutlet)] = data;
          this.selectOutlet(data);
          $('#new-outlet').modal('hide');
        })
        .catch((err) => {
          console.log(err);
          this.snackBar.open(err.error.message, 'Close', {
            verticalPosition: 'top'
          });
        });
      } else {

        this.restApi.postAuth('outlet/create', outletData)
        .then((data: any) => {
          this.router.navigate(['/distributionCenter/' + data._id]);
          this.outlets.push(data);
          this.selectOutlet(data);
          $('#new-outlet').modal('hide');
        })
        .catch((err) => {
          console.log(err);
          this.snackBar.open(err.error.message, 'Close', {
            verticalPosition: 'top'
          });
        });
      }
    
  }

  deleteOutlet(outlet) {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: 'Do you confirm the deletion of this user.'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.restApi.deleteAuth('outlet/' + outlet._id)
        .then(data => {
          this.outlets.splice(this.outlets.indexOf(outlet), 1);
          this.snackBar.open('Outlet successfully deleted.', 'Close', {
            verticalPosition: 'top'
          });
        })
        .catch(err => {
          this.snackBar.open('Error deleting user', 'Close', {
            verticalPosition: 'top'
          });
        });
      }
    });
  }

  editOutlet(outlet) {

    console.log(outlet);
    this.selectedOutlet = outlet;
    if (outlet) {
      const owner = outlet.owner.split(' ');
      let dt = new Date(outlet.date);

      this.newOutletForm.setValue({
        name: outlet.name,
        first_name: owner[0],
        last_name: owner[1],
        latitude: outlet.latitude,
        longitude: outlet.longitude,
        phone: outlet.phone,
        region: outlet.region._id,
        district: outlet.district._id,
        ward: outlet.ward._id,
        date: new FormControl(dt.toDateString(), Validators.required),
      });
      
      this.regionSelected();
      this.newOutletForm.patchValue({district: outlet.district._id});
      this.districtSelected();
      this.newOutletForm.patchValue({ward: outlet.ward._id});

      
    }
  }

  submitPosm() {
    let postData = this.PosmForm.value
    postData.outlet = this.selectedOutlet._id;
    this.restApi.postAuth('posms/create', postData)
      .then((data: any) => {
        this.posms.push(data);
        $('#posm-modal').hide();
        this.clearForms();
      })
      .catch((err: any) => {
        console.log(err);
        this.snackBar.open(err.error.message, 'Close', {
          verticalPosition: 'top'
        });
      });
  }

  deletePosm(posm) {
    this.restApi.deleteAuth('posms/' + posm._id)
      .then((data: any) => {
        this.posms.splice(this.posms.indexOf(posm),1);
      })
      .catch((err: any) => {
        console.log(err);
        this.snackBar.open(err.error.message, 'Close', {
          verticalPosition: 'top'
        });
      });
  }


}

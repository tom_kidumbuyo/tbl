import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DistributionCenterComponent } from './distribution-center.component';


const routes: Routes = [{
  path: 'distributionCenter',
  component: DistributionCenterComponent,
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class DistributionCenterRoutingModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DistributionCenterRoutingModule } from './distribution-center-routing.module';
import { SharedModule } from '../shared/shared.module';
import { DistributionCenterComponent } from './distribution-center.component';


@NgModule({
  declarations: [DistributionCenterComponent],
  imports: [
    CommonModule,
    DistributionCenterRoutingModule,
    SharedModule
  ]
})
export class DistributionCenterModule { }

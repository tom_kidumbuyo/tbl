import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressBarModule } from '@angular/material/progress-bar';


import { MorrisJsModule } from 'angular-morris-js';
import { ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';
import { MatNativeDateModule } from '@angular/material/core';
import { RouterModule, RouterLink } from '@angular/router';
import { OrderByPipe } from './_pipes/order-by.pipe';



@NgModule({
  declarations: [HeaderComponent, FooterComponent, ConfirmationDialogComponent, OrderByPipe],
  exports: [
    BrowserModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    HeaderComponent,
    FooterComponent,
    ConfirmationDialogComponent,
    MatCheckboxModule,
    MatRadioModule,
    MatFormFieldModule,
    MatSelectModule,
    MatChipsModule,
    MatIconModule,
    MatAutocompleteModule,
    MatInputModule,
    MatNativeDateModule,
    // MatDatepickerToggleModule,
    MatDatepickerModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    MatButtonModule,
    MatProgressBarModule,
    MorrisJsModule,
    OrderByPipe
    
  ],
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    MatCheckboxModule,
    MatRadioModule,
    MatFormFieldModule,
    MatSelectModule,
    MatChipsModule,
    MatIconModule,
    MatInputModule,
    MatAutocompleteModule,
    MatProgressBarModule,
    MatNativeDateModule,
    // MatDatepickerToggleModule,
    MatDatepickerModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    MatButtonModule,
    MorrisJsModule,

  ],
  entryComponents: [
    ConfirmationDialogComponent
  ],
})
export class SharedModule { }

import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/_services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  user: any = {};

  constructor(
    private auth: AuthenticationService,
    private router: Router
  ) {

  }

  ngOnInit() {
    this.auth.isLoggedIn()
    .then((data: any) => {
      this.user = data;
    })
    .catch(err => {
      console.log('Error getting user.');
    });
  }

  logOut() {
    this.auth.logout();
    this.router.navigate(['/auth']);
  }

}

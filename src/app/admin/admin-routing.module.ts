import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';

import { UsersComponent } from './users/users.component';
import { RegionsComponent } from './regions/regions.component';
import { LidmovementComponent } from './lidmovement/lidmovement.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PosComponent } from './pos/pos.component';


const routes: Routes = [{
  path: '',
  redirectTo: 'admin',
  pathMatch: 'full'
}, {
  path: 'admin',
  component: AdminComponent,
  children: [{
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  }, {
    path: 'dashboard',
    component: DashboardComponent
  }, {
    path: 'lidMovement',
    component: LidmovementComponent
  },  {
    path: 'admins',
    component: UsersComponent
  }, {
    path: 'regions',
    component: RegionsComponent
  }, {
    path: 'pos',
    component: PosComponent
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }

import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/_services/authentication.service';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { RestApiService } from 'src/app/_services/rest-api.service';
import { AdminService } from 'src/app/_services/admin.service';
import { Router } from '@angular/router';
import { ConfirmationDialogComponent } from 'src/app/shared/confirmation-dialog/confirmation-dialog.component';

declare var $: any;

@Component({
  selector: 'app-regions',
  templateUrl: './regions.component.html',
  styleUrls: ['./regions.component.scss']
})
export class RegionsComponent implements OnInit {

  regionSupervisorForm;
  selectedRegion;
  adminDataObserver: any;

  regions: any[];
  newRegionForm: any;
  users: any[];
  newDcForm: any;
  distributionCenters: any;
  outlets: any[] = [];
  selectedPage: string;

  constructor(
    private adminService: AdminService,
    private auth: AuthenticationService,
    private router: Router,
    private restApi: RestApiService,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    public dialog: MatDialog
  ) {

    this.adminDataObserver = this.adminService.getDataObservable();
    this.adminDataObserver.subscribe(data => {

    });
    this.adminService.setPage('regions');

    this.restApi.getAuth('admin/users')
    .then((data: any[]) => {
      this.users = data;
    })
    .catch(err => {
      this.snackBar.open('Error getting users' + err.error.message, 'Close', {
        verticalPosition: 'top'
      });
    });

    this.newRegionForm = this.formBuilder.group({
      name: new FormControl('', Validators.required),
      supervisor: new FormControl('', Validators.required),
      phone: new FormControl('', Validators.required),
      zone: new FormControl('North East', Validators.required)
    });

    this.newDcForm = this.formBuilder.group({
      id: new FormControl(false, Validators.required),
      name: new FormControl('', Validators.required),
      area: new FormControl('', Validators.required),
      //district: new FormControl('', Validators.required),
      //supervisor: new FormControl('', Validators.required),
      phone: new FormControl('', Validators.required)
    });

    this.restApi.getAuth('region')
    .then((data: any) => {
      console.log("REGIONS", data);
      this.regions = data;
      if (this.regions.length) {
        this.selectRegion(this.regions[0]);
      }
    })
    .catch((err) => {
      this.snackBar.open(err.error.message, 'Close', {
        verticalPosition: 'top'
      });
    });

    this.selectPage('dc');
  }

  ngOnInit() {
    this.auth.isLoggedIn()
    .then((data) => {
      console.log('loged in');
    })
    .catch(err => {
      this.router.navigate(['/auth']);
    });
  }

  selectRegion(region) {
    if(region.districts && region.districts.length) {
      this.newDcForm.controls.district.setValue(region.districts[0]._id);
    }

    console.log(this.newDcForm.controls);

    this.selectedRegion = region;
    this.restApi.getAuth('region/' + region._id + '/distributionCenters')
    .then((data) => {
      this.distributionCenters = data;
    })
    .catch((err) => {
      console.log(err);
      this.snackBar.open(err.error.message, 'Close', {
        verticalPosition: 'top'
      });
    });

    this.restApi.getAuth('region/' + region._id + '/outlets')
    .then((data: any[]) => {
      this.outlets = data;
    })
    .catch((err) => {
      console.log(err);
      this.snackBar.open(err.error.message, 'Close', {
        verticalPosition: 'top'
      });
    });

  }

  editDistributionCenter(distributionCenter) {
    if (distributionCenter) {
      this.newDcForm.controls.id.setValue(distributionCenter._id);
      distributionCenter.district ? this.newDcForm.controls.district.setValue(distributionCenter.district._id) : false ;
      distributionCenter.name ? this.newDcForm.controls.name.setValue(distributionCenter.name) : false ;
      distributionCenter.number ? this.newDcForm.controls.number.setValue(distributionCenter.number) : false ;
      distributionCenter.phone ? this.newDcForm.controls.phone.setValue(distributionCenter.phone) : false ;
    } else {
      this.newDcForm.controls.id.setValue(false);
      this.newDcForm.controls.name.setValue('');
      this.newDcForm.controls.number.setValue('');
      this.newDcForm.controls.phone.setValue('');
    }
  }

  deleteOutlet(outlet) {

  }

  deleteDistrict(district) {

  }

  selectRegionSupervisor() {
    this.restApi.postAuth('region/assign/' + this.selectedRegion._id , {

    })
    .then((data) => {

    })
    .catch((err) => {

    });
  }

  selectPage(page) {
    this.selectedPage = page;
  }

  createRegion() {
    this.restApi.postAuth('region/create', this.newRegionForm.value )
    .then((data) => {
      this.regions.push(data);
      $('#new-region').modal('hide');
    })
    .catch((err) => {
      console.log(err);
      this.snackBar.open(err.error.message, 'Close', {
        verticalPosition: 'top'
      });
    });
  }

  updateRegion() {

  }

  deleteRegion() {

  }

  createDistributionCenter() {

    const postData = this.newDcForm.value;
    postData.region = this.selectedRegion._id;

    if (postData.id) {
      this.restApi.putAuth('distributionCenter/' + postData.id, postData )
      .then((data: any) => {
        this.distributionCenters.push(data);
        $('#new-dc').modal('hide');
      })
      .catch((err) => {
        console.log(err);
        this.snackBar.open(err.error.message, 'Close', {
          verticalPosition: 'top'
        });
      });
    } else {
      this.restApi.postAuth('distributionCenter/create', postData )
      .then((data: any) => {
        this.distributionCenters.push(data);
        $('#new-dc').modal('hide');
      })
      .catch((err) => {
        console.log(err);
        this.snackBar.open(err.error.message, 'Close', {
          verticalPosition: 'top'
        });
      });
    }

  }

  updateDistributionCenter() {

  }

  deleteDistributionCenter(dc) {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: 'Do you confirm the deletion of this distribution center.'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.restApi.delete('distributionCenter/' + dc._id)
        .then((data: any) => {
          this.distributionCenters.splice(this.distributionCenters.indexOf(dc), 1);
        })
        .catch((err) => {
          console.log(err);
          this.snackBar.open(err.error.message, 'Close', {
            verticalPosition: 'top'
          });
        });
      }
    });
  }

}

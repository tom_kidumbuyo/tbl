import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AdminService } from 'src/app/_services/admin.service';
import { AuthenticationService } from 'src/app/_services/authentication.service';
import { RestApiService } from 'src/app/_services/rest-api.service';

declare var $: any;
declare var CanvasJS: any;

@Component({
  selector: 'app-pos',
  templateUrl: './pos.component.html',
  styleUrls: ['./pos.component.scss']
})
export class PosComponent implements OnInit {

  adminDataObserver: any;
  dispatchs: any;
  nationalReport: any = {
    regions: [],
    outlets: [],
    weeks: [],
    days: [],
    distributionCenters: [],
    endDate: new Date(),
    startDate: new Date(),
    crowns: {
      received: [],
      dispatched: [],
      received_amount: 0,
      dispatched_amount: 0
    }
  };
  selectedRegion: any = false;
  selectedDc: any = false;
  selectedOutlet: any = false;
  region_page: any = 'Dc';
  dc_page: any;
  outlet_page: any;


  d = new Date();
  hh = ('0' + Math.abs((this.d.getTimezoneOffset() - (this.d.getTimezoneOffset() % 60)) / 60)).slice(-2);
  mm = ('0' +  Math.abs(this.d.getTimezoneOffset() % 60)).slice(-2);
  tz = this.d.getTimezoneOffset() > 0 ? '-' + this.hh + this.mm : '+' + this.hh + this.mm;
  
  time = new Date();
  loadingMessage = '';
  dataLoaded: boolean = false;

  constructor(
    private adminService: AdminService,
    private auth: AuthenticationService,
    private router: Router,
    private restApi: RestApiService
  ) {

    this.adminDataObserver = this.adminService.getDataObservable();
    this.adminDataObserver.subscribe(data => {
      this.loadingMessage = data.loadingMessage;
      this.nationalReport = data.nationalReport;
      this.dataLoaded     = data.dataLoaded;
      if (this.dataLoaded) {
        setTimeout(() => {
          console.log('data loaded',this.nationalReport);
          this.renderChart();
        }, 3000);
      }
    });
    this.adminService.setPage('pos');

  }

  ngOnInit(): void {
  }

  renderChart() {

    let dataPoint = [{
      type: "column",	
		
      showInLegend: true,
      name: "Poster",
      dataPoints: this.nationalReport.days.map(day => {
        return {
          y: day.poster,
          label: day.date.toDateString()
        }
      })
    },{
      type: "column",	
		
      showInLegend: true,
      name: "Table Talker",
      dataPoints: this.nationalReport.days.map(day => {
        return {
          y: day.tableTalker,
          label: day.date.toDateString()
        }
      })
    },{
      type: "column",	
	
      showInLegend: true,
      name: "Sticker Counter",
      dataPoints: this.nationalReport.days.map(day => {
        return {
          y: day.stickerCounter,
          label: day.date.toDateString()
        }
      })
    },{
      type: "column",	
		 
      showInLegend: true,
      name: "Banners",
      dataPoints: this.nationalReport.days.map(day => {
        return {
          y: day.banners,
          label: day.date.toDateString()
        }
      })
    },{
      type: "column",	
	
      showInLegend: true,
      name: "Board Cutout",
      dataPoints: this.nationalReport.days.map(day => {
        return {
          y: day.boardCutOut,
          label: day.date.toDateString()
        }
      })
    }]

    var chart = new CanvasJS.Chart("posChartContainer", {
      animationEnabled: true,
      title:{
        text: ""
      },	
      axisY: {
        title: "POS",
        titleFontColor: "#4F81BC",
        lineColor: "#4F81BC",
        labelFontColor: "#4F81BC",
        tickColor: "#4F81BC"
      },
      axisY2: {
        title: "Day",
        titleFontColor: "#C0504E",
        lineColor: "#C0504E",
        labelFontColor: "#C0504E",
        tickColor: "#C0504E"
      },	
      toolTip: {
        shared: true
      },
      legend: {
        cursor:"pointer",
      },
      data: dataPoint
    });
    chart.render();

  }

  toolTipFormatter(e) {
    var str = "";
    var total = 0 ;
    var str3;
    var str2 ;
    for (var i = 0; i < e.entries.length; i++){
      var str1 = "<span style= \"color:"+e.entries[i].dataSeries.color + "\">" + e.entries[i].dataSeries.name + "</span>: <strong>"+  e.entries[i].dataPoint.y + "</strong> <br/>" ;
      total = e.entries[i].dataPoint.y + total;
      str = str.concat(str1);
    }
    str2 = "<strong>" + e.entries[0].dataPoint.label + "</strong> <br/>";
    str3 = "<span style = \"color:Tomato\">Total: </span><strong>" + total + "</strong><br/>";
    return (str2.concat(str)).concat(str3);
  }
  
  

}

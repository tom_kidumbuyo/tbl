import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { UsersComponent } from './users/users.component';

import { SharedModule } from '../shared/shared.module';
import { RegionsComponent } from './regions/regions.component';
import { LidmovementComponent } from './lidmovement/lidmovement.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PosComponent } from './pos/pos.component';


@NgModule({
  declarations: [AdminComponent, UsersComponent, DashboardComponent, RegionsComponent, LidmovementComponent, PosComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    SharedModule
  ]
})
export class AdminModule { }

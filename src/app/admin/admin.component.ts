import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../_services/authentication.service';
import { Router } from '@angular/router';
import { AdminService } from '../_services/admin.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  adminDataObserver: any;
  page: any;

  constructor(
    private adminService: AdminService,
    private auth: AuthenticationService,
    private router: Router
  ) {
    this.adminDataObserver = this.adminService.getDataObservable();
    this.adminDataObserver.subscribe((data: any) => {
      this.page = data.page;
    });
    console.log('admin component');
  }

  ngOnInit() {
    this.auth.isLoggedIn()
    .then((data: any) => {
      if (data.type == 'regionalsupervisor') {
        this.router.navigate(['/regionalsupervisor']);
      } else if (data.type == 'distributionCenter')  {
        this.router.navigate(['/distributionCenter']);
      } else if (data.type == 'aim')  {
        this.router.navigate(['/aim']);
      } else if (data.type == 'plant')  {
        this.router.navigate(['/plant']);
      }
    })
    .catch(err => {
      this.router.navigate(['/auth']);
    });
  }
}

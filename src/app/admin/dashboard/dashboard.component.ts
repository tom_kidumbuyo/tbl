import { Component, OnInit } from '@angular/core';
import { AdminService } from 'src/app/_services/admin.service';
import { AuthenticationService } from 'src/app/_services/authentication.service';
import { Router } from '@angular/router';
import { RestApiService } from 'src/app/_services/rest-api.service';

declare var $: any;
declare var CanvasJS: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  adminDataObserver: any;
  nationalReport: any = {
    regions: [],
    outlets: [],
    weeks: [],
    distributionCenters: [],
    endDate: new Date(),
    startDate: new Date(),
    crowns: {
      received: [],
      dispatched: [],
      received_amount: 0,
      dispatched_amount: 0
    },
    posms: [],
    giveaways: [],
    total_posms: {abs: 0, bunting: 0, paperposter: 0},
    total_giveaways: [],
  };


  dataLoaded: boolean = false;

  time = new Date();
  loadingMessage: any = '';

  constructor(
    private adminService: AdminService,
    private auth: AuthenticationService,
    private router: Router,
    private restApi: RestApiService
  ) {

    this.adminDataObserver = this.adminService.getDataObservable();
    this.adminDataObserver.subscribe((data: any) => {
      this.loadingMessage = data.loadingMessage;
      this.nationalReport = data.nationalReport;
      this.dataLoaded     = data.dataLoaded;
      if (this.dataLoaded === true) {
        setTimeout(() => {
          console.log('data loaded',this.nationalReport);
          this.refreshChart();
        }, 3000);
      }
    });
    this.adminService.setPage('dashboard');

  }

  ngOnInit() {
    this.auth.isLoggedIn()
    .then((data) => {
      console.log('loged in it ');
    })
    .catch(err => {
      this.router.navigate(['/auth']);
    });
  }

  refreshChart() {

      const regionChartData = this.nationalReport.regions.map(region => ({label: region.name, y: region.receives_amount}));
      const regionChart = new CanvasJS.Chart('RegionChartContainer', {
              animationEnabled: true,
              title: {
                text: '',
                horizontalAlign: 'right',
                fontFamily: 'Source Sans Pro'
              },
              data: [{
                type: 'doughnut',
                startAngle: 60,
                // innerRadius: 60,
                indexLabelFontSize: 17,
                indexLabel: '{label} - #percent%',
                toolTipContent: '<b>{label}:</b> {y} (#percent%)',
                dataPoints: regionChartData
              }]
      });
      regionChart.render();

      const dataPoints3 = [];
      let lastWeekStock = 0;
      // tslint:disable-next-line: max-line-length
      const dataPoints = this.nationalReport.weeks.map(week => {


        // let y = 0;
        // if ((week.sale_in_amount - week.sale_out_amount) > 0) {
        //   y = week.sale_in_amount - week.sale_out_amount;
        //   dataPoints3.push({label: 'week ' + week.name, y: (lastWeekStock * 20)});
        // } else {
        //   y = 0;
        //   dataPoints3.push({label: 'week ' + week.name, y: ((lastWeekStock + week.sale_in_amount - week.sale_out_amount) * 20)});
        // }


        // lastWeekStock = lastWeekStock + week.sale_in_amount - week.sale_out_amount;

        // return {label: 'week ' + week.name, y: (y * 20)};

        return {label: 'week ' + week.name, y:  week.sale_in_amount};

      });
      const dataPoints2 = this.nationalReport.weeks.map(week => ({label: 'week ' + week.name, y: (week.sale_out_amount * 20)}));

      const chart = new CanvasJS.Chart('chartContainer', {
          title: {
            text: '',
            horizontalAlign: 'right',
            fontFamily: 'Source Sans Pro'
          },
          axisY: {
            title: 'Number of Crowns',
            lineColor: '#4F81BC',
            tickColor: '#4F81BC',
            labelFontColor: '#4F81BC',
            gridThickness: 0
          },
          axisY2: {
            title: 'Sale out redemption rate',
            suffix: '%',
            gridThickness: 0,
            lineColor: '#C0504E',
            tickColor: '#C0504E',
            labelFontColor: '#C0504E'
          },
          data: [
          //   {
          //   type: 'stackedColumn',
          //   showInLegend: true,
          //   name: 'Carried in stock.',
          //   color: '#009432',
          //   dataPoints: dataPoints3
          // },
          {
            type: 'stackedColumn',
            showInLegend: true,
            // name: 'Unsold additional stock.',
            name: 'Sale in stock',
            color: '#3c40c6',
            dataPoints: dataPoints
          },
          // {
          //   type: 'stackedColumn',
          //   showInLegend: true,
          //   name: 'Sold out stock.',
          //   color: '#ff3f34',
          //   dataPoints: dataPoints2
          // }
        ]
      });
      chart.render();
      this.createPareto(chart);


  }


  createPareto(chart) {
    const dps = [];
    let yValue = 0;
    let yValue2 = 0;
    let yTotal = 0;
    let yPercent = 0;

    for (let i = 0; i < chart.data[0].dataPoints.length; i++) {
      yTotal += chart.data[0].dataPoints[i].y;
    }

    for (let i = 0; i < chart.data[0].dataPoints.length; i++) {
      yValue += this.nationalReport.weeks[i].received_amount;
      yValue2 += this.nationalReport.weeks[i].sale_out_amount * 10;
      yPercent = (yValue / yValue2 * 100);
      dps.push({label: chart.data[0].dataPoints[i].label, y: yPercent });
    }
    chart.addTo('data', {type: 'line', axisYType: 'secondary', yValueFormatString: '0.##"%"', indexLabel: '{y}',indexLabelBackgroundColor: "#34495e", indexLabelFontColor: '#ffffff', dataPoints: dps});
    chart.axisY[0].set('maximum', 10000000, false);
    chart.axisY2[0].set('maximum', 105, false );
    chart.axisY2[0].set('interval', 25 );
  }

  toggleDataSeries(e) {
    if (typeof(e.dataSeries.visible) === 'undefined' || e.dataSeries.visible) {
      e.dataSeries.visible = false;
    } else {
      e.dataSeries.visible = true;
    }
  }

}

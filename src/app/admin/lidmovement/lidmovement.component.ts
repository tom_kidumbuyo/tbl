import { Component, OnInit } from '@angular/core';
import { AdminService } from 'src/app/_services/admin.service';
import { AuthenticationService } from 'src/app/_services/authentication.service';
import { Router } from '@angular/router';
import { RestApiService } from 'src/app/_services/rest-api.service';

@Component({
  selector: 'app-lidmovement',
  templateUrl: './lidmovement.component.html',
  styleUrls: ['./lidmovement.component.scss']
})
export class LidmovementComponent implements OnInit {

  adminDataObserver: any;
  dispatchs: any;
  nationalReport: any = {
    regions: [],
    outlets: [],
    weeks: [],
    distributionCenters: [],
    endDate: new Date(),
    startDate: new Date(),
    crowns: {
      received: [],
      dispatched: [],
      received_amount: 0,
      dispatched_amount: 0
    }
  };
  selectedRegion: any = false;
  selectedDc: any = false;
  selectedOutlet: any = false;
  region_page: any = 'Dc';
  dc_page: any;
  outlet_page: any;


  d = new Date();
  hh = ('0' + Math.abs((this.d.getTimezoneOffset() - (this.d.getTimezoneOffset() % 60)) / 60)).slice(-2);
  mm = ('0' +  Math.abs(this.d.getTimezoneOffset() % 60)).slice(-2);
  tz = this.d.getTimezoneOffset() > 0 ? '-' + this.hh + this.mm : '+' + this.hh + this.mm;
  
  loadingMessage = '';
  dataLoaded: boolean = false;

  constructor(
    private adminService: AdminService,
    private auth: AuthenticationService,
    private router: Router,
    private restApi: RestApiService
  ) {

    this.adminDataObserver = this.adminService.getDataObservable();
    this.adminDataObserver.subscribe(data => {
      this.loadingMessage = data.loadingMessage;
      this.nationalReport = data.nationalReport;
      this.dataLoaded     = data.dataLoaded;
      if (this.dataLoaded) {
        this.selectRegion(this.nationalReport.regions[0]);
      }
    });
    this.adminService.setPage('lidmovement');

  }

  ngOnInit() {

  }

  selectRegion(region) {
    this.selectedRegion = region;
    this.select_region_page('summary');
    this.selectDc(false);
    this.selectOutlet(false);
  }

  select_region_page(page) {
    this.region_page = page;
  }

  selectDc(Dc) {
    this.selectedDc = Dc;
    this.select_dc_page('summary');
  }

  select_dc_page(page) {
    this.dc_page = page;
  }

  selectOutlet(outlet) {
    this.selectedOutlet = outlet;
    this.select_outlet_page('summary');
  }

  select_outlet_page(page) {
    this.outlet_page = page;
  }

}

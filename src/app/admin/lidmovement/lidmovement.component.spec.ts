import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LidmovementComponent } from './lidmovement.component';

describe('LidmovementComponent', () => {
  let component: LidmovementComponent;
  let fixture: ComponentFixture<LidmovementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LidmovementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LidmovementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

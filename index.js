//MARK: --- REQUIRE MODULES
const port = process.env.PORT || 8080;
const express = require('express');
const expressApp = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
const compression = require('compression')


const dotenv = require('dotenv');
dotenv.config();

expressApp.use(compression())
expressApp.use(bodyParser.urlencoded({ extended: true }));

// Create link to Angular build directory
var distDir = __dirname + "/dist";
expressApp.use(express.static(distDir));

var originsWhitelist = [
  'http://localhost:4200',
  'http://127.0.01:4200'
];

var corsOptions = {
  origin: function(origin, callback){
        var isWhitelisted = originsWhitelist.indexOf(origin) !== -1;
        callback(null, isWhitelisted);
  },
  credentials: false
};
//here is the magic
expressApp.use(cors(corsOptions));

// import route files
const authRoutes = require('./routes/auth');
const assetsRoutes = require('./routes/assets');
const userRoutes = require('./routes/user');
const adminRoutes = require('./routes/admin');
const distributionCenterRoutes = require('./routes/distributionCenter');
const regionalSupervisorRoutes = require('./routes/regionalSupervisor');
const lidsMovement = require('./routes/lidsMovement');
const outlet = require('./routes/outlet');
const comment = require('./routes/comment');
const sales = require('./routes/sales');
const plant = require('./routes/plant');
const posm = require('./routes/posm');
const location = require('./routes/location');

// use routes
expressApp.use('/api/auth', authRoutes);
expressApp.use('/api/assets', assetsRoutes);
expressApp.use('/api/user', userRoutes);
expressApp.use('/api/admin', adminRoutes);
expressApp.use('/api/distributionCenter', distributionCenterRoutes);
expressApp.use('/api/region', regionalSupervisorRoutes);
expressApp.use('/api/lidsMovement', lidsMovement);
expressApp.use('/api/outlet', outlet);
expressApp.use('/api/comment', comment);
expressApp.use('/api/sales', sales);
expressApp.use('/api/plant', plant);
expressApp.use('/api/outlet', outlet);
expressApp.use('/api/posms', posm);
expressApp.use('/api/location', location);


// redirects to angular
expressApp.use(function(req, res) {
  console.log(req.url);
  console.log(__dirname + '/dist/index.html');
  res.sendFile(__dirname + '/dist/index.html'); // will execute angular code
});

mongoose.connection.on("open", function(ref) {
  console.log("Connected to mongoDb server.");
});

mongoose.connection.on("error", function(err) {
  console.log("Could not connect to mongo server!");
  return console.log(err);
});


db = mongoose.connect('mongodb+srv://root:' + process.env.DB_PASSWORD + '@cluster0.pnv64.mongodb.net/amplify?retryWrites=true&w=majority', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  socketTimeoutMS: 30000,
  keepAlive: true,
});

//init the server
expressApp.listen(port, () => {
   console.log(`listening on port ${port}`);
});

module.exports = expressApp;



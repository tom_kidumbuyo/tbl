const express = require('express');
const router = express.Router();
const auth = require('../libraries/auth');
const regionModel = require('../models/region');
const userModel = require('../models/user')
const lidMovementModel = require('../models/lid_movement');
const districtModel = require('../models/district');
const saleModel = require('../models/sale');
const distributionCenterModel = require('../models/distribution_center');
const outletModel = require('../models/outlet');
const wardModel = require('../models/ward');
const neatCsv = require('neat-csv');
const fs = require('fs')


router.get('/csv', (req, res) => {

  fs.readFile('routes/dcs.csv', async (err, data) => {
    if (err) {
      console.error(err)
      return
    }
    csvs = await neatCsv(data)
    for (let csv of csvs) {

      region = await regionModel.findOne({
        name: csv.Region.toLowerCase()
      })
      if (!region && csv.Region) {
        region = await regionModel.create({
          name: csv.Region.toLowerCase()
        })
      }

      district = await districtModel.findOne({
        name: csv.District.toLowerCase(),
        region: region,
      })
      if (!district && csv.District) {
        district = await districtModel.create({
          name: csv.District.toLowerCase(),
          region: region,
        })
      }

      number = csv.Dc.split(" ", 1)[0];
      console.log("\n\n NUMBER", number)

      distributionCenter = await distributionCenterModel.findOne({
        number: number,
        name: csv.Dc.toLowerCase().replace(number,""),
        region: region,
        district: district
      })
      if (!distributionCenter && csv.Dc) {
        distributionCenter = await distributionCenterModel.create({
          number: number,
          name: csv.Dc.toLowerCase().replace(number,""),
          region: region,
          district: district,
        })
      }

      console.log('csv', csv);

      if (csv.email_1) {
      let usr = await userModel.findOne({
        email: csv.email_1.toLowerCase()
      })

      //create user and assign the username
      if (!usr && csv.email_1) {
        await auth.register(csv.email_1.toLowerCase(),csv.phone_1.toLowerCase(),csv.phone_1.toLowerCase(),req)
        .then(async (data) => {

            try {
              user = await userModel.updateOne({_id:data.user._id},{$set: {
                first_name: csv.name_1.split(" ")[0].toLowerCase(),
                last_name: csv.name_1.split(" ")[1].toLowerCase(),
                type: 'distributionCenter',
                dc: distributionCenter._id,
              }})
              usr = await userModel.findById(data.user._id);
            } catch (err) {
              console.log('signup error: ',err);
            }

        })
        .catch(err => {
          console.log('Error creation user', err);
        });
      } else {
        usr.type = 'distributionCenter';
        usr.dc = distributionCenter;
        await usr.save();
      }
      }



      if (csv.email_2) {
      usr = await userModel.findOne({
        email: csv.email_2.toLowerCase()
      })

      //create user and assign the username
      if (!usr && csv.email_2) {
        await auth.register(csv.email_2.toLowerCase(),csv.phone_2.toLowerCase(),csv.phone_2.toLowerCase(),req)
        .then(async (data) => {

            try {
              user = await userModel.updateOne({_id:data.user._id},{$set: {
                first_name: csv.name_2.split(" ")[0].toLowerCase(),
                last_name: csv.name_2.split(" ")[1].toLowerCase(),
                type: 'distributionCenter',
                dc: distributionCenter._id,
              }})
              usr = await userModel.findById(data.user._id);
            } catch (err) {
              console.log(err)
            }

        })
        .catch(err => {
          console.log('Error creation user', err);
        });
      } else {
        usr.type = 'distributionCenter';
        usr.dc = distributionCenter;
        await usr.save();
      }
      }



      console.log(distributionCenter);

    }
    res.json(csvs)
  })
})

router.get('/csv2', (req, res) => {
  fs.readFile('routes/pocs.csv', async (err, data) => {
    if (err) {
      console.error(err)
      return
    }

    csvs = await neatCsv(data)
    for (let csv of csvs) {

      console.log(csv)

      region = await regionModel.findOne({
        name: csv.REGION.toLowerCase()
      })
      if (!region) {
        region = await regionModel.create({
          name: csv.REGION.toLowerCase()
        })
      }

      district = await districtModel.findOne({
        name: csv.LOCATION.toLowerCase(),
        region: region,
      })
      if (!district) {
        district = await districtModel.create({
          name: csv.LOCATION.toLowerCase(),
          region: region,
        })
      }

      outlet  = await outletModel.findOne({
        number: csv['POC ID']
      })

      if (!outlet) {
        outlet = await outletModel.create({
          number: csv['POC ID'],
          name: csv['POC NAME'].toLowerCase(),
          owner: csv['BDR NAME '].toLowerCase(),
          region: region,
          district: district,
        })
      }

      console.log(csv)

    }
    res.json(csvs);

  })
})

router.get('/csv3', (req, res) => {
  fs.readFile('routes/wards.csv', async (err, data) => {
    if (err) {
      console.error(err)
      return
    }
    csvs = await neatCsv(data)
    for (let csv of csvs) {
      console.log(csv)

      region = await regionModel.findOne({
        name: csv.Reg_Name
      })
      if (!region) {
        region = await regionModel.create({
          name: csv.Reg_Name
        })
      }

      district = await districtModel.findOne({
        name: csv.Dis_Name,
        region: region,
      })
      if (!district) {
        district = await districtModel.create({
          name: csv.Dis_Name,
          region: region,
        })
      }

      ward = await wardModel.findOne({
        name: csv.Ward_Name,
        district: district,
      })
      if (!ward) {
        ward = await wardModel.create({
          name: csv.Ward_Name,
          district: district,
        })
      }

    }
    res.json(csvs);

  })
})


router.post('/create', async (req, res) => {

  try{

    distributionCenter = await distributionCenterModel.create({
      area: req.body.area,
      region: req.body.region,
      name: req.body.name,
      phone: req.body.phone,
    })

    res.json(distributionCenter);

  } catch(err) {

    console.log(err);
    res.status(500).json({error: err});

  }

})

router.put('/:id', async (req, res) => {
  try{
    distributionCenter = await distributionCenterModel.findById(req.params.id);
    distributionCenter.area = req.body.area;
    distributionCenter.region = req.body.region;
    distributionCenter.name = req.body.name;
    distributionCenter.phone = req.body.phone;
    await distributionCenter.save();
    res.json(distributionCenter);
  } catch(err) {
    console.log(err);
    req.status(500).json({'status': 'err'});
  }
})



router.get('/lids', async (req, res) => {
  try {
    received = await lidMovementModel.find({
      to: 'distributionCenter',
      from: 'outlet'
    })
    dispatched = await lidMovementModel.find({
      from: 'distributionCenter',
      to: 'plant'
    })
    res.json({
      received: received,
      dispatched: dispatched
    })
  } catch (error) {
      res.status(500).json({error: error});
  }
});

router.get('/sales', async (req, res) => {
  try {
    saleIn = await saleModel.find({
      type: 'in'
    })
    saleOut = await saleModel.find({
      type: 'out'
    })
    res.json({
      in: saleIn,
      out: saleOut
    })
  } catch (error) {
    res.status(500).json({error: error});
  }
});

router.delete('/:id', async (req, res) => {
  try{
    distributionCenter = await distributionCenterModel.findById(req.params.id)
    await distributionCenter.delete();
    res.json({'status': 'success'})
  } catch(err) {
    console.log(err)
    res.status(500).json({'status': 'err'})
  }
})

router.get('/:id', async (req, res) => {
  distributionCenter = await distributionCenterModel.findById(req.params.id)
  try {
    distributionCenter = await distributionCenterModel.findById(req.params.id).populate('region').populate('supervisor');
    res.json(distributionCenter);
  } catch(err) {
    console.log('error',err)
    res.status(500).json({error: err});
  }
})


router.get('/', (req, res) => {

})

router.use('/:id/lidMovement/received', auth.verify);
router.get('/:id/lidMovement/received', async (req, res) => {
  lidMovement = await lidMovementModel.find({

  })
  res.json(lidMovement);
})

router.use('/:id/lidMovement/dispatched', auth.verify);
router.get('/:id/lidMovement/dispatched', async (req, res) => {
  lidMovement = await lidMovementModel.find({

  })
  res.json(lidMovement);
})

module.exports = router;


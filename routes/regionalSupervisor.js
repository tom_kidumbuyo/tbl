const express = require('express');
const router = express.Router();
const regionModel = require('../models/region');
const distributionCenterModel = require('../models/distribution_center');
const userModel = require('../models/user')
const outletsModel = require('../models/outlet')
const districtModel = require('../models/district')
const saleModel = require('../models/sale')
const outletModel = require('../models/outlet')
const zoneModel = require('../models/zone')
const auth = require('../libraries/auth');
const zone = require('../models/zone');

router.use('/create', auth.verify);
router.post('/create', async (req, res ) => {
  try {

    // if(req.body.supervisor && req.body.supervisor != '') {
    //   regions = await regionModel.find({
    //     supervisor: req.body.supervisor
    //   })

    //   for (region of regions) {
    //     region.supervisor = null;
    //     await region.save();
    //   }

    //   distributionCenters = await distributionCenterModel.find({
    //     supervisor: req.body.supervisor
    //   })

    //   for (distributionCenter of distributionCenters) {
    //     distributionCenter.supervisor = null;
    //     await distributionCenter.save();
    //   }
    // }

    rzone = await zoneModel.find({
      name: req.body.zone
    })

    if(!rzone || rzone.length == 0){
      rzone = await zoneModel.create({
        name: req.body.zone
      })
    } else {
      rzone = rzone[0];
    }


    region = await regionModel.create({
      name: req.body.name,
      //supervisor: req.body.supervisor,
      zone: rzone
    });

    // if(req.body.supervisor && req.body.supervisor != '') {
    //   user = await userModel.findById(req.body.supervisor);
    //   user.region = region;
    //   user.type = 'regionalSuperviser'
    //   await user.save();
    // }

    res.json(region);

  } catch (error) {
    console.log(error);
    res.status(500).json({error: error})
  }
})



router.use('/assign/:id', auth.verify);
router.post('/assign/:id', async (req, res ) => {
  try {
    region = await regionModel.findById(req.params.id);
    region.supervisor = req.body.userId
    await region.save();
    res.json(region)
  } catch (error) {
    res.status(500).json({error: error})
  }
})

router.use('/', auth.verify);
router.get('/', async (req, res ) => {
  try {

    regions = await regionModel.find({}).populate('zone');
    rgs = [];
    for (let region of regions) {
      rg = region.toObject();
      rg.districts = await districtModel.find({region: region})
      rg.distributionCenters = await distributionCenterModel.find({region: region});
      rgs.push(rg)
    }
    res.json(rgs);
  } catch (error) {
    console.log(error);
    res.status(500).json({error: err});
  }

  
})

router.use('/:id/distributionCenters', auth.verify);
router.get('/:id/distributionCenters', async (req, res ) => {
  distributionCenters = await distributionCenterModel.find({
    region: req.params.id
  }).populate('supervisor').populate('region').populate('district');
  res.json(distributionCenters)
})

router.use('/:id', auth.verify);
router.get('/:id', async (req, res ) => {
  try {

    region = await regionModel.findById(req.params.id)
    if (region) {
      rg = region.toObject();
      rg.districts = await districtModel.find({region: region});
      rg.distributionCenters = await distributionCenterModel.find({region: region});
      res.json(rg);
    } else {
      res.status(500).json({error: 'No region with that id'})
    }

  } catch (error) {
    console.log(error)
    res.status(500).json({error: error})
  }

})

router.use('/:id/outlets', auth.verify);
router.get('/:id/outlets', async (req, res ) => {
  outlets = await outletsModel.find({
    region: req.params.id
  }).populate('region').populate('district')
  res.json(outlets)
})

router.use('/:id/districts', auth.verify);
router.get('/:id/districts', async (req, res ) => {
  districts = await districtModel.find({
    region: req.params.id
  })
  res.json(districts)
})

router.use('/:id/report', auth.verify);
router.get('/:id/report', async (req, res) => {
try {

  console.log('\n\n\n\n start report')

  region = await regionModel.findById(req.params.id);

  rg = region.toObject();
  rg.distributionCenters = []
  rg.districts = await districtModel.find({
    region: region
  })

  distributionCenters = await distributionCenterModel.find({region: region}).populate('supervisor');
  for (const dc of distributionCenters) {
        console.log('DC', dc);

        let distributionCenter = dc.toObject();
        distributionCenter.terms = await userModel.find({
          dc: dc._id
        })

        let lidMovements = await lidMovementModel.find({
          distributionCenter: distributionCenter._id
        }).populate('outlet').populate('distributionCenter')

        distributionCenter.receives = [];
        distributionCenter.dispatchs = [];

        for (let lidMovement of lidMovements) {
          if (lidMovement.from == 'outlet' && lidMovement.to == 'distributionCenter') {
            distributionCenter.receives.push(lidMovement)
          } else if (lidMovement.from == 'distributionCenter' && lidMovement.to == 'plant') {
            distributionCenter.dispatchs.push(lidMovement)
          }
        }

        let sales = await saleModel.find({
          distributionCenter: distributionCenter._id
        }).populate('outlet')

        distributionCenter.sale_in = [];
        distributionCenter.sale_out = [];

        for (let sale of sales) {
          if (sale.type == 'in') {
            distributionCenter.sale_in.push(sale);
          } else if (sale.type == 'out') {
            distributionCenter.sale_out.push(sale);
          }
        }
        if (rg.distributionCenters == undefined) {
          rg.distributionCenters = [];
        }
        rg.distributionCenters.push(distributionCenter);
  }

  rg.outlets = []
  outlets = await outletModel.find({region: region});
  for (const outlet of outlets) {
        ol = outlet.toObject();
        ol.receives = await lidMovementModel.find({
          to: 'distributionCenter',
          from: 'outlet',
          outlet: outlet._id
        }).populate('distributionCenter');
        ol.sale_in = await saleModel.find({
          type: 'in',
          outlet: outlet._id
        })

        if (rg.outlets != undefined) {
          rg.outlets.push(ol);
        } else {
          rg.outlets = [];
          rg.outlets.push(ol)
        }
  }
  res.json(rg);
} catch(err) {
  console.log(err)
  res.status(500).json({
    error: err
  });
}

})

module.exports = router;

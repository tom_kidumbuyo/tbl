const express = require('express');
const router = express.Router();
const outletModel = require('../models/outlet')
const auth = require('../libraries/auth');
const posmModel = require('../models/posm');
const outlet = require('../models/outlet');

router.use('/', auth.verify);
router.get('/', async (req, res ) => {
  try {
    outlets = await outletModel.find({}).populate('district').populate('region').populate('ward')
    res.json(outlets);
  } catch (error) {
    console.log(error);
    res.status(500).json({error: error})
  }
});

router.use('/create', auth.verify);
router.post('/create', async (req, res ) => {
  try {
    outlet = await outletModel.create({
      name: req.body.name,
      owner: req.body.first_name + ' ' + req.body.last_name,
      phone: req.body.phone,
      region: req.body.region,
      ward: req.body.ward,
      longitude: req.body.longitude,
      latitude: req.body.latitude,
      district: req.body.district != "" ? req.body.district : null,
      town: req.body.town != "" ? req.body.town : null
    })
    outlet = await outlet.populate('district').populate('region').populate('ward');
    res.json(outlet);
  } catch (error) {
    console.log(error);
    res.status(500).json({error: error})
  }
});

router.use('/:id', auth.verify);
router.put('/:id', async (req, res ) => {
  try {
    outlet = await outletModel.findById(req.params.id);
    outlet.name = req.body.name;
    outlet.owner = req.body.first_name + ' ' + req.body.last_name;
    outlet.phone = req.body.phone;
    outlet.region = req.body.region;
    outlet.district = req.body.district;
    outlet.town = req.body.town;
    outlet.updated = Date.now();
    await outlet.save()
    outlet = await outlet.populate('region').populate('district').populate('ward');
    res.json(outlet);
  } catch (error) {
    console.log(error);
    res.status(500).json({error: error})
  }
});

router.get('/:id/posms', async (req, res) => {
  try {
    posms = await posmModel.find({outlet: req.params.id}).populate('user');
    res.json(posms);
  } catch (err) {
    console.log(err);
    res.status(500).json({status: 'error',error: 'error deleting outlet'});
  }
});

router.delete('/:id', async (req, res) => {
  try {
    outlet = await outletModel.findById(req.params.id);
    await outlet.delete();
    res.json({status: 'success'});
  } catch (err) {
    console.log(err);
    res.status(500).json({status: 'error', error: err});
  }
});

module.exports = router;

const express = require('express');
const router = express.Router();
const countries = require("i18n-iso-countries");
const userModel = require('../models/user');
const auth = require('../libraries/auth');

router.get('/countries', (req, res ) => {
  res.json(countries.getNames("en"));
});


router.get('/adminExists', (req, res ) => {
  userModel.find({
    type: 'admin',
  })
  .exec((err, data) => {
    if (err || !data.length) {
      res.json({status: false});
      return
    }
    res.json({status: true});
  });
});

module.exports = router;

const express = require('express');
const router = express.Router();
const auth = require('../libraries/auth');
const districtModel = require('../models/district');
const wardModel = require('../models/ward');


router.get('/districts', async (req, res ) => {
  try {
    districts = await districtModel.find({});
    res.json(districts);
  } catch (error) {
    res.status(500).json({error: error})
  }
});


router.get('/wards', async (req, res ) => {
  try {
    wards = await wardModel.find({});
    res.json(wards);
  } catch (error) {
    res.status(500).json({error: error})
  }
});

module.exports = router;


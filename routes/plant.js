const express = require('express');
const router = express.Router();
const auth = require('../libraries/auth');
const lidMovementModel = require('../models/lid_movement')


router.use('/dispatchs', auth.verify);
router.get('/dispatchs', async (req, res) => {
  lidMovement = await lidMovementModel.find({
    to : 'plant'
  })
  .populate('distributionCenter')
  res.json(lidMovement);
})

router.use('/verifyDispatch/:id', auth.verify);
router.post('/verifyDispatch/:id', async (req, res) => {

  lidMovement = await lidMovementModel.findById(req.params.id);
  lidMovement.verified_amount = 0;
  lidMovement.verified = true;

  for (let box of lidMovement.boxes) {
    for (let bx of req.body.boxes) {
      if (box.number == bx.number) {
        box.plant_amount = bx.amount
        lidMovement.verified_amount += box.plant_amount;
      }
    }
  }
  await lidMovement.save();
  await lidMovement.populate('distributionCenter');

  res.json(lidMovement);

})

module.exports = router;

const express = require('express');
const router = express.Router();
const auth = require('../libraries/auth');
lidMovementModel = require('../models/lid_movement');

router.post('/create', auth.verify);
router.post('/create', async (req, res) => {
  verified = true
  if (req.body.to == 'plant') {
    verified = false;
  }
  try {

    let boxes = [];
    if (req.body.boxes) {
      boxes = req.body.boxes
    }

    lidMovement = await lidMovementModel.create({
      from: req.body.from,
      to: req.body.to,
      amount: req.body.amount,
      verified: verified,
      distributionCenter: req.body.distributionCenter,
      // outlet:  req.body.outlet,
      vehicle_number: req.body.vehicle_number,
      status: req.body.status,
      boxes: req.body.boxes,
      date: req.body.date ? req.body.date : null,
    })
    res.json(lidMovement)
  } catch (error) {
    console.log(error);
    res.status(500).json({error: error});
  }
})

router.post('/verify/:id', auth.verify);
router.post('/verify/:id', async(req, res) => {
  try {
    user = req.user.toObject();
    lidMovement = await lidMovementModel.findById(req.params.id);
    lidMovement.verified = false;
    lidMovementModel.verified_by = user._id;
    await lidMovementModel.save();
    res.json(lidMovementModel)
  } catch (error) {
    res.status(500).json({error: error})
  }
})

router.post('/:id', auth.verify);
router.delete('/:id', async(req, res) => {
  try {
    lidMovement = await lidMovementModel.findById(req.params.id);
    lidMovement.delete();
    res.json({status: 'success'})
  } catch (error) {
    console.log(error)
    res.status(500).json({status: 'error', error: error})
  }
})

module.exports = router;

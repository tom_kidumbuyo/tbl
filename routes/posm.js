const express = require('express');
const router = express.Router();
const posmModel = require('../models/posm');
const auth = require('../libraries/auth');

router.use('/create', auth.verify);
router.post('/create', async (req, res ) => {
  try {
    posm = await posmModel.create({
      outlet: req.body.outlet,
      foundPoster: req.body.foundPoster,
      newPoster: req.body.newPoster,
      foundTableTalker: req.body.foundTableTalker,
      newTableTalker: req.body.newTableTalker,
      foundStickerCounter: req.body.foundStickerCounter,
      newStickerCounter: req.body.newStickerCounter,
      foundBanners : req.body.foundBanners,
      newBanners : req.body.newBanners,
      foundBoardCutOut : req.body.foundBoardCutOut,
      newBoardCutOut : req.body.newBoardCutOut,
    })
    res.json(posm);
  } catch (error) {
    console.log(error);
    res.status(500).json({error: error})
  }
})

router.use('/:id', auth.verify);
router.put('/:id', async (req, res ) => {
  try {

    posm = await posmModel.findById(req.params.id);
    posm.foundPoster = req.body.foundPoster;
    posm.newPoster = req.body.newPoster;
    posm.foundTableTalker = req.body.foundTableTalker;
    posm.newTableTalker = req.body.newTableTalker;
    posm.foundStickerCounter = req.body.foundStickerCounter;
    posm.foundBanners  = req.foundBanners;
    posm.foundBoardCutOut  = req.foundBoardCutOut;
    posm.newBoardCutOut  = req.newBoardCutOut;
    await posm.save()
    res.json(posm);

  } catch (error) {
    console.log(error);
    res.status(500).json({error: error})
  }
})

router.get('/:id', async (req, res) => {
  try {
    posm = await posmModel.findById(req.params.id);
    res.json(posm );
  } catch (err) {
    console.log(err);
    res.status(500).json({status: 'error',error: 'error deleting posm'});
  }
});

router.delete('/:id', async (req, res) => {
  try {
    posm = await posmModel.findById(req.params.id);
    await posm.delete();
    res.json({status: 'success'});
  } catch (err) {
    console.log(err);
    res.status(500).json({status: 'error',error: 'error deleting posm'});
  }
});

module.exports = router;

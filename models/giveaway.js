
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const giveawaySchema = new Schema({
    amount: { type: Number, require: true },
    // product: {
    //   type: Schema.ObjectId,
    //   ref: 'product',
    // },
    outlet: {
      type: Schema.ObjectId,
      ref: 'outlet',
    },
    date: {type: Date, required: true, default: Date()}
});

module.exports = mongoose.model('giveaway', giveawaySchema, 'giveaways');

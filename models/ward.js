
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const wardSchema = new Schema({
    name: { type: String, require: true },
    district: {
        type: Schema.ObjectId,
        ref: 'district',
    },
});

module.exports = mongoose.model('ward', wardSchema, 'wards');

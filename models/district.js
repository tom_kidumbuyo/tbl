
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const districtSchema = new Schema({
    name: { type: String, require: true },
    region: {
      type: Schema.ObjectId,
      ref: 'region',
    },
});

module.exports = mongoose.model('district', districtSchema, 'districts');

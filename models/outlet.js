
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const outletSchema = new Schema({
    number: { type: String },
    name: { type: String, require: true },
    owner: { type: String, require: true },
    phone: { type: String },
    classification: { type: String, enum: ['','mainstream','premium','club']},
    seats: { type: Number},
    amenities: [{
      type: String,
      enum: ['kitchen', 'DSTV', 'pool table', 'DJ']
    }],
    counters:  { type: Number },
    longitude: { type: String },
    latitude: { type: String },
    region: {
      type: Schema.ObjectId,
      ref: 'region',
    },
    district: {
      type: Schema.ObjectId,
      ref: 'district',
    },
    ward: {
      type: Schema.ObjectId,
      ref: 'ward',
    },
    town: { type: String},
    created: { type: Date, default: Date.now()},
    updated: { type: Date, default: Date.now()},
});

module.exports = mongoose.model('outlet', outletSchema, 'outlets');


const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const posmSchema = new Schema({
    foundPoster: { type: Number, require: true },
    newPoster: { type: Number, require: true },
    foundTableTalker: { type: Number, require: true },
    newTableTalker: { type: Number, require: true },
    foundStickerCounter: { type: Number, require: true },
    newStickerCounter: { type: Number, require: true },
    foundBanners: { type: Number, require: true },
    newBanners: { type: Number, require: true },
    foundBoardCutOut: { type: Number, require: true },
    newBoardCutOut: { type: Number, require: true },
    date: {type: Date, required: true, default: Date()},
    outlet: {
      type: Schema.ObjectId,
      ref: 'outlet',
    },
    user: {
      type: Schema.ObjectId,
      ref: 'user',
    },
});

module.exports = mongoose.model('posm', posmSchema, 'posms');

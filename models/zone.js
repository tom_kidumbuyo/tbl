
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const zoneSchema = new Schema({
    name: { type: String, require: true },
});

module.exports = mongoose.model('zone', zoneSchema, 'zones');

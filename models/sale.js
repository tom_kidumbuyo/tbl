
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const saleSchema = new Schema({
    cases: { type: String, require: true },
    type: { type: String, require: true, enum: ['in', 'out'] },
    distributionCenter: {
      type: Schema.ObjectId,
      ref: 'distributionCenter',
    },
    outlet: {
      type: Schema.ObjectId,
      ref: 'outlet',
    },
    date: {type: Date, required: true, default: Date()}
});

module.exports = mongoose.model('sale', saleSchema, 'sales');

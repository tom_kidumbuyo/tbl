
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const regionSchema = new Schema({
    name: { type: String, require: true },
    supervisor:  {
      type: Schema.ObjectId,
      ref: 'user',
    },
    zone:  {
      type: Schema.ObjectId,
      ref: 'zone',
    },
});

module.exports = mongoose.model('region', regionSchema, 'regions');
